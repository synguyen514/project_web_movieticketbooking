import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BaseService} from '../shared/services/base.service';
import {Inject, Injectable} from '@angular/core';
import {baseUrlAPI} from '../shared/services/baseUrl';
import {map, Observable} from 'rxjs';
import {IGenre} from "../models/genre-models";
import {ICinema} from "../models/cinema-model";

@Injectable({providedIn: 'root'})
export class CinemaService extends BaseService {
  public readonly LocalData: string = 'MOVIES_DATA_KEY';

  private readonly endpointApiUrls = {
    getAll: 'get-all',
  };

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrlAPI) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api/cinema-management');
  }

  public getAllCinema(token: string): Observable<ICinema[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient
      .get<ICinema[]>(
        this.createUrl(this.endpointApiUrls.getAll),
        {headers}
      )
      .pipe(map((response) => response as ICinema[]));
  }
}
