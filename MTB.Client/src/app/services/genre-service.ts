import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BaseService} from '../shared/services/base.service';
import {Inject, Injectable} from '@angular/core';
import {baseUrlAPI} from '../shared/services/baseUrl';
import {map, Observable} from 'rxjs';
import {IGenre} from "../models/genre-models";

@Injectable({providedIn: 'root'})
export class GenreService extends BaseService {
  public readonly LocalData: string = 'MOVIES_DATA_KEY';

  private readonly endpointApiUrls = {
    getAll: 'get-all',
  };

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrlAPI) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api/genre-management');
  }

  public getAllGenre(token: string): Observable<IGenre[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient
      .get<IGenre[]>(
        this.createUrl(this.endpointApiUrls.getAll),
        {headers}
      )
      .pipe(map((response) => response as IGenre[]));
  }
}
