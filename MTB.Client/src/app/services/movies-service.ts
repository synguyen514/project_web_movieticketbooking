import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BaseService} from '../shared/services/base.service';
import {Inject, Injectable} from '@angular/core';
import {baseUrlAPI} from '../shared/services/baseUrl';
import {map, Observable} from 'rxjs';
import {IMovie} from "../models/movie-model";

@Injectable({providedIn: 'root'})
export class MoviesService extends BaseService {
  public readonly LocalData: string = 'MOVIES_DATA_KEY';

  private readonly endpointApiUrls = {
    getAll: 'get-all',
    getAllByGenre: 'get-all-by-genre',
  };

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrlAPI) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api/movie-management');
  }

  public getAllMovies(token: string): Observable<IMovie[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient
      .get<IMovie[]>(
        this.createUrl(this.endpointApiUrls.getAll),
        {headers}
      )
      .pipe(map((response) => response as IMovie[]));
  }

  public getAllMoviesByGenre(token: string, genreId: string): Observable<IMovie[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient
      .get<IMovie[]>(
        this.createUrl(`${this.endpointApiUrls.getAllByGenre}/${genreId}`),
        {headers}
      )
      .pipe(map((response) => response as IMovie[]));
  }
}
