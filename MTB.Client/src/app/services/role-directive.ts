// import {Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
// import {Role} from '../../../shared/enums/roleEnums';
// import {RoleService} from '../../../services/role-service';
//
// @Directive({
//   standalone: true,
//   selector: '[appRole]'
// })
// export class RoleDirective implements OnInit {
//   private currentRole: Role;
//
//   constructor(
//     private roleService: RoleService,
//     private templateRef: TemplateRef<any>,
//     private viewContainer: ViewContainerRef
//   ) {
//   }
//
//   ngOnInit() {
//     this.currentRole = this.roleService.getRoleId();
//     this.updateView();
//   }
//
//   @Input('appRole') roles: Role[]; // Input để nhận danh sách các vai trò
//
//   private updateView() {
//     if (this.roles && this.roles.includes(this.currentRole)) {
//       this.viewContainer.createEmbeddedView(this.templateRef);
//     } else {
//       this.viewContainer.clear();
//     }
//   }
// }
