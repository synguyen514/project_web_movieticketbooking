import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BaseService} from '../shared/services/base.service';
import {Inject, Injectable} from '@angular/core';
import {baseUrlAPI} from '../shared/services/baseUrl';
import {map, Observable} from 'rxjs';
import {IGenre} from "../models/genre-models";
import {ICinema} from "../models/cinema-model";
import {IUserUpdateImage, IUserUpdatePassWord} from "../models/user-model";
import {MessageResponse} from "../models/response-models";

@Injectable({providedIn: 'root'})
export class UserService extends BaseService {
  public readonly LocalData: string = 'MOVIES_DATA_KEY';

  private readonly endpointApiUrls = {
    getAll: 'get-all',
    changePassWord: 'update-password',
    updateImage: 'update-image',
  };

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrlAPI) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api/user-management');
  }

  public getAllCinema(token: string): Observable<ICinema[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient
      .get<ICinema[]>(
        this.createUrl(this.endpointApiUrls.getAll),
        {headers}
      )
      .pipe(map((response) => response as ICinema[]));
  }

  public updateUser(user: IUserUpdatePassWord, token: string): Observable<MessageResponse> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });

    return this.httpClient
      .put<MessageResponse>(this.createUrl(this.endpointApiUrls.changePassWord),
        user, { headers });
  }

  public updateImageUser(user: IUserUpdateImage, token: string): Observable<MessageResponse> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });

    return this.httpClient
      .put<MessageResponse>(this.createUrl(this.endpointApiUrls.updateImage),
        user, { headers });
  }
}
