// import {Injectable} from '@angular/core';
// import {CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot} from '@angular/router';
// import {RoleService} from "../../../services/role-service";
//
// @Injectable({
//   providedIn: 'root'
// })
// export class RoleGuard implements CanActivate {
//
//   constructor(
//     private router: Router,
//     private roleService: RoleService,
//   ) {
//   }
//
//   canActivate(
//     route: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot
//   ): boolean {
//     const expectedRoleIds = route.data['roleIds'] as string[];
//     const currentRoleId = this.roleService.getRoleId().toString()
//
//     if (currentRoleId && expectedRoleIds && expectedRoleIds.includes(currentRoleId)) {
//       return true;
//     } else {
//       this.router.navigate(['/notfound']);
//       return false;
//     }
//   }
// }
