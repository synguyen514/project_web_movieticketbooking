import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BaseService} from '../shared/services/base.service';
import {Inject, Injectable} from '@angular/core';
import {baseUrlAPI} from '../shared/services/baseUrl';
import {map, Observable} from 'rxjs';
import {IGenre} from "../models/genre-models";
import {ITicket, ITicketAdd} from "../models/ticket-models";
import {MessageResponse} from "../models/response-models";

@Injectable({providedIn: 'root'})
export class TicketService extends BaseService {
  public readonly LocalData: string = 'MOVIES_DATA_KEY';

  private readonly endpointApiUrls = {
    getAll: 'get-all',
    getAllByUserId: 'get-all-by-userId',
    add: 'add',
  };

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrlAPI) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api/ticket-management');
  }

  public getAllTickets(token: string): Observable<ITicket[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient
      .get<ITicket[]>(
        this.createUrl(this.endpointApiUrls.getAll),
        {headers}
      )
      .pipe(map((response) => response as ITicket[]));
  }

  public getTicketByUserId(id: string | undefined, token: string): Observable<ITicket[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });

    return this.httpClient
      .get<ITicket[]>(this.createUrl(`${this.endpointApiUrls.getAllByUserId}/${id}`), {headers})
      .pipe(map((response) => response as ITicket[]));
  }

  public createTicket(request: ITicketAdd, token: string): Observable<MessageResponse> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
    return this.httpClient
      .post<MessageResponse>(this.createUrl(this.endpointApiUrls.add), request, { headers })
      .pipe(map((response) => response as MessageResponse));
  }
}
