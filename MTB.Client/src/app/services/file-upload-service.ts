import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BaseService} from "../shared/services/base.service";
import {Inject, Injectable} from "@angular/core";
import {baseUrlAPI} from "../shared/services/baseUrl";
import {map, Observable} from "rxjs";
import {FileUploadResponse} from "../models/response-models";

@Injectable({providedIn: "root"})
export class FileUploadService extends BaseService {

  private readonly fileUploadApiUrls = {
    fileUpload: 'upload'
  };

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrlAPI) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api/file-management');
  }

  public uploadFiles(token: string, file: File): Observable<FileUploadResponse> {
    const formData = new FormData();
    formData.append('file', file, file.name);
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });

    return this.httpClient.post<FileUploadResponse>(
      this.createUrl(this.fileUploadApiUrls.fileUpload), formData, {headers: headers});
  }

}
