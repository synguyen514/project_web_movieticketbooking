import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BaseService} from '../shared/services/base.service';
import {Inject, Injectable} from '@angular/core';
import {baseUrlAPI} from '../shared/services/baseUrl';
import {map, Observable} from 'rxjs';
import {IMovie} from "../models/movie-model";
import {IPayment, IPaymentResponse} from "../models/payment-model";

@Injectable({providedIn: 'root'})
export class VnpayService extends BaseService {

  private readonly endpointApiUrls = {
    createPayment: 'create_payment_url',
  };

  constructor(
    httpClient: HttpClient,
    @Inject(baseUrlAPI) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api/Payment');
  }

  public createPaymentUrl(payment: IPayment): Observable<IPaymentResponse> {
    return this.httpClient
      .post<IPaymentResponse>(this.createUrl(this.endpointApiUrls.createPayment), payment)
      .pipe(map((response) => response as IPaymentResponse));
  }

}
