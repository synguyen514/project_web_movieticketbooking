import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Router} from "@angular/router";
import {BaseService} from "../shared/services/base.service";
import {BehaviorSubject, catchError, map, Observable, tap, throwError} from "rxjs";
import {Inject, Injectable} from "@angular/core";
import {baseUrlAPI} from "../shared/services/baseUrl";
import {
  IAuthTokens,
  ILoginModel, ILoginOptions
} from "../models/authentication-model";
import {IUser, User} from "../models/user-model";
import {MessageResponse, OTPResponse} from "../models/response-models";

@Injectable({providedIn: "root"})
export class AuthService extends BaseService {
  public readonly accessTokenKey: string = 'ACCESS_TOKEN';
  public readonly refreshTokenKey: string = 'REFRESH_TOKEN';
  public readonly userDataKey: string = 'USER_INFO_DATA';

  // @ts-ignore
  public currentUser: BehaviorSubject<IUser> = new BehaviorSubject<IUser>(null);

  private readonly authApiUrls = {
    validateJwtToken: 'validateJwtToken',
    renewJwtToken: 'RenewToken',
    logout: 'logout',
    refreshTokens: 'refreshToken',
    logIn: 'login',
    sendOTP: 'send-otp',
  };

  constructor(
    httpClient: HttpClient,
    private readonly router: Router,
    @Inject(baseUrlAPI) protected hostUrl: string
  ) {
    super(httpClient);
    this.setEndpoint(hostUrl, 'api/authentication-management');
  }

  isLoggedIn(): boolean {
    return !!this.getAccessToken();
  }

  public sendOTPForMail(email: string): Observable<OTPResponse> {
    const params = new HttpParams().set('mail', email);

    return this.httpClient
      .post<OTPResponse>(this.createUrl(this.authApiUrls.sendOTP), null,{ params })
      .pipe(map((response) => response as OTPResponse));
  }

  public isAccessTokenExpired(): boolean {
    try {
      const accessToken = this.getAccessToken();
      const expiry =
        accessToken && JSON.parse(atob(accessToken.split('.')[1])).exp;

      return Math.floor(new Date().getTime() / 1000) >= expiry;
    } catch (e) {
      return true;
    }
  }

  public refreshTokens(loginOptions?: ILoginOptions): Observable<any> {
    const savedRefreshToken = this.getRefreshToken();
    const options = {
      headers: new HttpHeaders({
        Authorization: `Bearer ${savedRefreshToken}`,
      }),
    };

    return this.httpClient
      .post(this.authApiUrls.refreshTokens, null, options)
      .pipe(
        tap(({data}: any) => {
          const {accessToken, refreshToken} = data;

          this.setTokens(accessToken, refreshToken);
        }),
        catchError((err: any) => {
          localStorage.clear()
          this.redirectToLogin(loginOptions);

          return throwError(err);
        })
      );
  }

  public login(loginInfo: ILoginModel): Observable<IAuthTokens> {
    return this.httpClient
      .post<IAuthTokens>(
        this.createUrl(this.authApiUrls.logIn),
        loginInfo
      );
  }

  public redirectToLogin(options?: ILoginOptions) {
    console.log('---------------REDIRECT_TO_LOGIN---------------')
    this.router.navigate(['/auth/sign-in']).then();
  }

  public redirectToUrl(url: string): void {
    this.router.navigate([url]).then();
  }

  public logout(id: string): Observable<MessageResponse> {
    localStorage.clear();
    this.router.navigate(['']);
    return this.httpClient.post<MessageResponse>(this.createUrl(`${this.authApiUrls.logout}/${id}`), null);
  }

  public mapDataLogin(response: IAuthTokens) {
    localStorage.clear();
    if (response && response.user) {
      this.setUserData(response.user);
      this.setTokens(response.accessToken, response.refreshToken)
    } else {
      console.error('Invalid user data provided.');
    }
  }

  public getUserData(): IUser | null {
    const userDataString = localStorage.getItem(this.userDataKey);
    if (userDataString) {
      try {
        return JSON.parse(userDataString) as IUser;
      } catch (error) {
        console.error('Error parsing user data from localStorage:', error);
        return null;
      }
    } else {
      return null;
    }
  }

  public updateUserProperty(property: keyof IUser, value: any): void {
    const userData = this.getUserData();
    if (userData) {
      // @ts-ignore
      userData[property] = value; // Cập nhật thuộc tính
      localStorage.setItem(this.userDataKey, JSON.stringify(userData)); // Lưu lại vào localStorage
    } else {
      console.error('User data not found in localStorage.');
    }
  }

  public getAccessToken(): string {
    // @ts-ignore
    return localStorage.getItem(this.accessTokenKey);
  }

  public getRefreshToken(): string {
    // @ts-ignore
    return localStorage.getItem(this.refreshTokenKey);
  }

  public setTokens(accessToken: string, refreshToken: string): void {
    this.setAccessToken(accessToken);
    this.setRefreshToken(refreshToken);
  }

  public setUserData(userInfo: IUser): void {
    if (userInfo) {
      const userDataString = JSON.stringify(userInfo);
      localStorage.setItem(this.userDataKey, userDataString);
    } else {
      console.error('Invalid user info provided.');
    }
  }

  private setAccessToken(token: string): void {
    localStorage.setItem(this.accessTokenKey, token);
  }

  private setRefreshToken(token: string): void {
    localStorage.setItem(this.refreshTokenKey, token);
  }

}
