import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {SvgIconComponent} from "./shared/components/svg-icon/svg-icon.component";
import {SvgIconsComponent} from "./shared/components/svg-icons/svg-icons.component";

@Component({
  standalone: true,
  imports: [
    RouterOutlet,
    SvgIconComponent,
    SvgIconsComponent,
  ],
  providers: [
  ],
  selector: 'app-root',
  template: `
    <router-outlet></router-outlet>
    <svg-icons></svg-icons>
  `,
  styles: [`

  `]
})
export class AppComponent {
  title = 'app';
}
