export interface MessageResponse {
  isSuccess: boolean;
  message: string;
}

export interface MessageResponseAddImportExport {
  importExportId: string;
  isSuccess: boolean;
  message: string;
}

export interface OTPResponse {
  defaultPassword: string;
  isSuccess: boolean;
  message: string;
}

export interface FileUploadResponse {
  isSuccess: boolean;
  message: string;
  imageId: string;
}
