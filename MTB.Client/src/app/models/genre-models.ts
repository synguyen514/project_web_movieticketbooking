import {IMovie} from "./movie-model";

export interface IGenre {
  id: number;
  genreName: string;
  isDeleted: boolean;
}

export interface IGenreMovie {
  name: string;
  movies: IMovie[];
}
