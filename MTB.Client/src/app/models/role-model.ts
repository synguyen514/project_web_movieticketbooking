export interface RoleResponse {
  id: string;
  roleName: string;
  createdAt: string;
  modifiedAt: string;
  isDeleted: boolean;
  subId: number;
}
