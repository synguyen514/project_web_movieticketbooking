export interface IMovie {
  id: number;
  movieName: string;
  yearOfManufacture: string;
  releaseDate: string;
  description: string;
  imageId?: number | null;
  imageNameUrl?: string;
  trailerUrl?: string;
  genreId?: number | null;
  countryId?: number | null;
}
