
export interface ITicket {
  id: number;
  userId: number;
  movieId: number;
  movieName: string;
  cinemaId: number;
  cinemaName: string;
  dateOrder: string;
  price: number;
  seatNumber: number;
  hallNumber: number;
  status: string;
  isDeleted: boolean;
}

export interface ITicketAdd {
  userId: number;
  movieId: number;
  cinemaId: number;
  seatNumber: number;
}
