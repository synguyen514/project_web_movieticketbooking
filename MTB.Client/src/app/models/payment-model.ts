export interface IPayment {
  amount: number;
  orderInfo: string;
  bankCode: string;
}

export interface IPaymentResponse {
  paymentUrl: string;
}
