export interface IUser {
  imageName: string;
  roleName: string;
  id: number;
  fullName: string;
  dob: string;
  gender: boolean;
  address: string;
  phone: string;
  email: string;
  password: string;
  roleId: number;
}

export class User {
  imageName: string;
  roleName: string;
  id: number;
  fullName: string;
  dob: string;
  gender: boolean;
  address: string;
  phone: string;
  email: string;
  password: string;
  roleId: number;

  constructor(
    imageName: string,
    roleName: string,
    id: number,
    fullName: string,
    dob: string,
    gender: boolean,
    address: string,
    phone: string,
    email: string,
    password: string,
    roleId: number,
  ) {
    this.id = id;
    this.fullName = fullName;
    this.dob = dob;
    this.gender = gender;
    this.address = address;
    this.phone = phone;
    this.email = email;
    this.password = password;
    this.roleId = roleId;
    this.fullName = fullName;
    this.imageName = imageName;
    this.roleName = roleName;
  }
}

export interface IUserUpdatePassWord {
  email: string;
  password: string;
}

export interface IUserUpdateImage {
  id: number;
  imageId: number;
}
