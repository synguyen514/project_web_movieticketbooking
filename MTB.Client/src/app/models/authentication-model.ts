import {Params} from '@angular/router';
import {IUser} from "./user-model";

export interface IAuthTokens {
  accessToken: string;
  refreshToken: string;
  user: IUser;
}

export interface ILoginOptions extends Params {
  silently?: boolean;
  redirectUrl?: string;
}

export interface ILoginModel {
  email: string;
  password: string;
}

export interface IForgotPasswordModel {
  key: string;
}
