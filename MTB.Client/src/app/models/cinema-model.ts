export interface ICinema {
  id: number;
  cinemaName: string;
  address: string;
  isDeleted: boolean;
}
