import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../services/authentication-service";
import {ImageService} from "../../services/path-image.service";
import {NzBadgeComponent} from "ng-zorro-antd/badge";
import {NzDropDownDirective, NzDropdownMenuComponent} from "ng-zorro-antd/dropdown";
import {NzMenuDirective, NzMenuItemComponent} from "ng-zorro-antd/menu";
import {RouterLink} from "@angular/router";
import {NzIconDirective} from "ng-zorro-antd/icon";
import {ConfirmModalComponent} from "../../components/confirm-modal/confirm-modal.component";
import {NgIf} from "@angular/common";
import {SvgIconComponent} from "../../components/svg-icon/svg-icon.component";
import {NzButtonComponent} from "ng-zorro-antd/button";

@Component({
  selector: 'app-nav-bar',
  standalone: true,
  imports: [
    NzBadgeComponent,
    NzDropDownDirective,
    NzDropdownMenuComponent,
    NzMenuDirective,
    NzMenuItemComponent,
    RouterLink,
    NzIconDirective,
    ConfirmModalComponent,
    NgIf,
    SvgIconComponent,
    NzButtonComponent
  ],
  templateUrl: './nav-bar.component.html',
  styleUrl: './nav-bar.component.scss'
})
export class NavBarComponent implements OnInit{
  showDropdown: boolean = false;
  // @ts-ignore
  username: string = "";
  imageUrl: string = "";
  isLogin = this.authService.isLoggedIn();
  isAdmin = this.authService.getUserData()?.roleId === 1;

  constructor(
    private authService: AuthService,
    private imageService: ImageService,
  ) {

  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      // @ts-ignore
      this.username = this.authService.getUserData().fullName.toString();
      // @ts-ignore
      this.imageUrl = this.imageService.getImageUrl(this.authService.getUserData().imageName);
    }
  }

  toggleDropdown() {
    this.showDropdown = !this.showDropdown;
  }

  handleLogout(): void {
    // @ts-ignore
    this.authService.logout(this.authService.getUserData().id);
  }
}
