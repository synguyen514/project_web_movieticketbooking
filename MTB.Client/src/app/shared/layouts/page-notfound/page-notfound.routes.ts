import { Route } from '@angular/router';
import {PageNotFoundComponent} from "./page-notfound.component";

export const routes: Route[] = [
  {
    path: 'notfound',
    loadComponent: () =>
      import('./page-notfound.component').then((c) => c.PageNotFoundComponent),
  }
];

