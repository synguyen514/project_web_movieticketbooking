import {Component} from '@angular/core';
import {SvgIconComponent} from "../../components/svg-icon/svg-icon.component";

@Component({
  selector: 'app-page-not-found',
  standalone: true,
  imports: [
    SvgIconComponent
  ],
  templateUrl: './page-notfound.component.html',
  styleUrl: './page-notfound.component.scss'
})
export class PageNotFoundComponent {

}
