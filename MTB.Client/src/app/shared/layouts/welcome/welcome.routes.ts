import { Route } from '@angular/router';
import {WelcomeComponent} from "./welcome.component";

export const routes: Route[] = [
  {
    path: '',
    loadComponent: () =>
      import('./welcome.component').then((c) => c.WelcomeComponent),
  }
];

