import {Component} from '@angular/core';
import {SvgIconComponent} from "../../components/svg-icon/svg-icon.component";

@Component({
  selector: 'app-welcome',
  standalone: true,
  imports: [
    SvgIconComponent
  ],
  templateUrl: './welcome.component.html',
  styleUrl: './welcome.component.scss'
})
export class WelcomeComponent {

}
