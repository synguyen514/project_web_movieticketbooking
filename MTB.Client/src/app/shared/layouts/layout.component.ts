import {Component, Input} from '@angular/core';
import {RouterModule} from '@angular/router';
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import {NzMenuModule} from 'ng-zorro-antd/menu'
import {NgForOf} from "@angular/common";
import {NzTypographyModule} from "ng-zorro-antd/typography";


@Component({
  standalone: true,
  imports: [
    RouterModule,
    NzLayoutModule,
    NzBreadCrumbModule,
    NzMenuModule,
    NgForOf,
    NzTypographyModule,
  ],
  selector: 'app-layout',
  template: `
    <nz-layout>
      <nz-layout>
        <nz-layout>
          <nz-layout class="inner-layout">
            <ng-content></ng-content>
          </nz-layout>
        </nz-layout>
      </nz-layout>
    </nz-layout>
  `,

  styles: [`

  `]
})
export class LayoutComponent {
  @Input() breadcrumbItems: string[] = [];
  @Input() titlePage: string = '';
}
