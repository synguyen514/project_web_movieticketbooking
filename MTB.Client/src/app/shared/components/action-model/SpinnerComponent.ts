import { Component, Input } from '@angular/core';
import {NzSpinComponent} from "ng-zorro-antd/spin";
import {NzAlertComponent} from "ng-zorro-antd/alert";

@Component({
  selector: 'app-spinner',
  template: `
    <nz-spin [nzSpinning]="loading">
    </nz-spin>
  `,
  standalone: true,
  imports: [
    NzSpinComponent,
    NzAlertComponent
  ],
  styles: []
})
export class SpinnerComponent {
  @Input() loading = false;
}
