import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';

@Component({
  selector: 'svg-icon',
  templateUrl: 'svg-icon.component.html',
  standalone: true,
  imports: [CommonModule]
})
export class SvgIconComponent {
  // @ts-ignore
  @Input() name: string;
  @Input() size: number | string = 16;
  @Input() width?: number | string;
  @Input() height?: number | string;
  @Input() fill?: string;
  @Input() stroke?: string;
  // @ts-ignore
  @Input() className: string;
  window: any = window;

  constructor() {
  }

  get iconUrl() {
    return `${this.window.location.href}#${this.name}`;
  }
}
