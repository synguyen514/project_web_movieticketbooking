import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';

@Component({
  selector: 'svg-icons',
  templateUrl: './svg-icons.component.html',
  standalone: true,
  imports: [CommonModule]
})
export class SvgIconsComponent {
}
