import { Injectable } from '@angular/core';
import { PresetRanges } from 'ng-zorro-antd/date-picker';

@Injectable({
  providedIn: 'root'
})
export class DateRangeComponent {
  public dateRanges: PresetRanges;

  constructor() {
    this.initDateRanges();
  }

  private initDateRanges(): void {
    const today = new Date();
    const dayOfWeek = today.getDay();
    const daysToSubtractDay = (dayOfWeek + 6) % 7;
    const daysToSubtractWeek = (dayOfWeek + 6) % 7 + 7;

    this.dateRanges = {
      'Hôm nay': [new Date(today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0), new Date()],
      'Hôm qua': [new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1, 0, 0, 0), new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1, 23, 59, 59)],
      'Tuần này (Thứ Hai - Hôm nay)': [this.getPreviousDate(daysToSubtractDay), new Date()],
      '7 ngày trước': [new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7, 0, 0, 0), new Date()],
      'Tuần trước (Thứ Hai - Hôm nay)': [this.getPreviousDate(daysToSubtractWeek), this.getPreviousDate(daysToSubtractWeek - 6)],
      '14 ngày trước': [new Date(today.getFullYear(), today.getMonth(), today.getDate() - 14, 0, 0, 0), new Date()],
      'Tháng này': [new Date(today.getFullYear(), today.getMonth(), 1), new Date()],
      '30 ngày trước': [new Date(today.getFullYear(), today.getMonth(), today.getDate() - 30, 0, 0, 0), new Date()],
      'Tháng trước': [new Date(today.getFullYear(), today.getMonth() - 1, 1), new Date(today.getFullYear(), today.getMonth(), 0)]
    };
  }

  private getPreviousDate(n: number): Date {
    const date = new Date();
    date.setDate(date.getDate() - n);
    return date;
  }
}
