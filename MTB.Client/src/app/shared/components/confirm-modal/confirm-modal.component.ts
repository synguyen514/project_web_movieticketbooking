import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SvgIconComponent} from "../svg-icon/svg-icon.component";
import {NzModalModule} from "ng-zorro-antd/modal";
import { NzButtonModule } from 'ng-zorro-antd/button';
import {NgClass, NgForOf, NgIf} from "@angular/common";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  ReactiveFormsModule
} from "@angular/forms";
import {NzDatePickerComponent} from "ng-zorro-antd/date-picker";
import {NzTimePickerComponent} from "ng-zorro-antd/time-picker";
import {NzOptionComponent, NzSelectComponent} from "ng-zorro-antd/select";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {AuthService} from "../../../services/authentication-service";
import {MoviesService} from "../../../services/movies-service";
import {GenreService} from "../../../services/genre-service";
import {VnpayService} from "../../../services/payment-service";
import {DomSanitizer} from "@angular/platform-browser";
import {CinemaService} from "../../../services/cinema-service";
import {ICinema} from "../../../models/cinema-model";
import {TicketService} from "../../../services/ticket-service";
import {ITicketAdd} from "../../../models/ticket-models";
import {Router} from "@angular/router";
import {IPayment} from "../../../models/payment-model";
@Component({
  selector: 'app-confirm-modal',
  standalone: true,
  imports: [
    SvgIconComponent,
    NzModalModule,
    NzButtonModule,
    NgClass,
    NgIf,
    ReactiveFormsModule,
    NzDatePickerComponent,
    NzTimePickerComponent,
    NzSelectComponent,
    NzOptionComponent,
    NgForOf
  ],
  providers: [

  ],
  templateUrl: './confirm-modal.component.html',
  styleUrl: './confirm-modal.component.scss'
})
export class ConfirmModalComponent implements OnInit{
  @Input() isVisible = false;
  @Input() iconName: string = '';
  @Input() movieName: string = '';
  @Input() movieId: number = 0;
  @Input() isDanger: boolean = true;
  @Output() isVisibleChange = new EventEmitter<boolean>();
  @Output() confirm = new EventEmitter<void>();
  @Output() cancel = new EventEmitter<void>();
  accessToken = this.authService.getAccessToken();

  // @ts-ignore
  bookingForm: FormGroup;
  cinemas: ICinema[] = [];
  // @ts-ignore
  ticketData: ITicketAdd;
  // @ts-ignore
  paymentData: IPayment;
  // @ts-ignore
  selectedPaymentMethod: string = '';
  seats = Array.from({ length: 50 }, (_, i) => ({ id: i + 1, number: i + 1 }));

  constructor(
    private fb: FormBuilder,
    private cinemaService: CinemaService,
    private authService: AuthService,
    private ticketService: TicketService,
    private router: Router,
    private notification: NzNotificationService,
    private cdRef: ChangeDetectorRef,
    private vnpayService: VnpayService,
    private sanitizer: DomSanitizer
  ) {

  }

  onCancel() {
    this.isVisible = false;
    this.isVisibleChange.emit(this.isVisible);
    this.cancel.emit();
  }

  onConfirm() {
    this.isVisible = false;
    this.isVisibleChange.emit(this.isVisible);
    this.confirm.emit();

    this.ticketData = {
      userId : <number>this.authService.getUserData()?.id,
      movieId : this.movieId,
      cinemaId : this.bookingForm.value.cinema,
      seatNumber : <number>this.getFirstSelectedSeatIndex()
    }
    this.createTicket(this.ticketData, this.accessToken);
  }

  ngOnInit(): void {
    this.bookingForm = this.fb.group({
      cinema: [null],
      seats: this.fb.array(this.seats.map(seat => this.fb.control(false)))
    });

    this.getMoviesData(this.accessToken);
  }

  public getMoviesData(token: string) {
    this.cinemaService.getAllCinema(token).subscribe(
      (data) => {
        this.cinemas = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  get seatsArray(): FormArray {
    return this.bookingForm.get('seats') as FormArray;
  }

  // @ts-ignore
  seatControl(index: number): FormControl {
    return this.seatsArray.at(index) as FormControl;
  }

  getFirstSelectedSeatIndex(): number | null {
    for (let i = 0; i < this.seatsArray.length; i++) {
      if (this.seatsArray.at(i).value) {
        return i;
      }
    }
    return null;
  }

  selectPaymentMethod(method: string): void {
    this.selectedPaymentMethod = method;
  }

  public createTicket(ticket: ITicketAdd, token: string) {

    this.paymentData = {
      amount: 120000,
      orderInfo: this.movieName,
      bankCode: this.selectedPaymentMethod
    }

    this.ticketService.createTicket(ticket, token).subscribe(
      (response) => {
        if (response.isSuccess) {
          this.notification.success('Đặt vé thành công',
            'Chuyển hướng đến trang thanh toán sau 3s');
            this.vnpayService.createPaymentUrl(this.paymentData).subscribe(response => {
              window.open(response.paymentUrl, '_blank')
            });
            this.cdRef.detectChanges();
        } else {
          this.notification.error('Lỗi', response.message);
        }
      },
      (error) => {
        this.notification.error('Lỗi', 'Đã có lỗi xảy ra trong quá trình hoạt động');
      }
    );
  }

  onSubmit() {
    console.log(this.bookingForm.value);
  }

}
