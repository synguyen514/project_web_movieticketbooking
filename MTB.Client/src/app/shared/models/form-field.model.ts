export interface IFormFieldError {
  type: string;
  message: string;
  params?: any;
}
export interface Person {
  key: string;
  name: string;
  age: number;
  address: string;
}
