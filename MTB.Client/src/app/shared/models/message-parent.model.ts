export interface MessageParent {
    messageName: string;
    data: any
}

export const MessageParentName = {
    REDIRECT_TO: 'REDIRECT_TO',
    REMOVE_LOCAL_STORAGE: 'REMOVE_LOCAL_STORAGE',
    LOAD_SUCCESS: 'LOAD_SUCCESS',
    NOTICE_SUCCESS: 'NOTICE_SUCCESS'
};