import {Injectable} from '@angular/core';
import {MessageParent, MessageParentName} from "../../models/message-parent.model";

@Injectable({providedIn: 'root'})
export class IframeMessageService {

    constructor() {
    }

    redirectToPage(url: string){
        const messageObject = {
            messageName: MessageParentName.REDIRECT_TO,
            data: {
                url: url
            }
        } as MessageParent;
        const messageString = JSON.stringify(messageObject);
        window.parent.postMessage(messageString, '*');
    }
    noticeSuccess(mess: string){
        const messageObject = {
            messageName: MessageParentName.NOTICE_SUCCESS,
            data: {
                mess: mess
            }
        } as MessageParent;
        const messageString = JSON.stringify(messageObject);
        console.log(messageObject);
        window.parent.postMessage(messageString, '*');
    }
    sendLoadSuccess(){
        const messageObject = {
            messageName: MessageParentName.LOAD_SUCCESS,
            data: {}
        } as MessageParent;
        const messageString = JSON.stringify(messageObject);
        window.parent.postMessage(messageString, '*');
    }
}
