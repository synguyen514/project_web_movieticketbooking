﻿import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  constructor(private router: Router) {}

  // reloadCurrentPage() {
  //   this.router.navigate([this.router.url]).then(() => {
  //     window.location.reload();
  //     console.log("reload")
  //   });
  // }

  reloadCurrentPage() {
    const url = this.router.url.split('?');
    const path = url[0];
    const queryParams = this.router.parseUrl(this.router.url).queryParams;

    this.router.navigate([path], { queryParams }).then(() => {
      window.location.reload();
      console.log("reload");
    });
  }

  generateRandomBarcode(length: number): string {
    let result = '';
    const characters = '0123456789';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

}
