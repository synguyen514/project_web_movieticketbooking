import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  private baseImageUrl = '/assets/images/';

  constructor() { }

  getImageUrl(imageName: string): string {
    return this.baseImageUrl + imageName;
  }
}
