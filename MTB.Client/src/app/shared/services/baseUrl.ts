import { InjectionToken } from '@angular/core';
import {environment} from "../environments/environment";

export const baseUrlAPI = new InjectionToken<string>('baseUrlAPI', {
  providedIn: 'root',
  factory: () => environment.baseUrlAPI,
});
