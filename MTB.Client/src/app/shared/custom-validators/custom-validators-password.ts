import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export class CustomValidators {
  static passwordComplexity(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const password = control.value;
      const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;
      return passwordRegex.test(password) ? null : { passwordComplexity: 'Mật khẩu phải bao gồm ít nhất 6 ký tự, chữ hoa, chữ thường, số và ký tự đặc biệt.' };
    };
  }

  static passwordMatch(passwordKey: string, confirmPasswordKey: string): ValidatorFn {
    return (formGroup: AbstractControl): ValidationErrors | null => {
      const password = formGroup.get(passwordKey)?.value;
      const confirmPassword = formGroup.get(confirmPasswordKey)?.value;
      return password === confirmPassword ? null : { passwordMismatch: 'Mật khẩu xác nhận không khớp.' };
    };
  }

  static differentFromOldPassword(oldPasswordKey: string, newPasswordKey: string): ValidatorFn {
    return (formGroup: AbstractControl): ValidationErrors | null => {
      const oldPassword = formGroup.get(oldPasswordKey)?.value;
      const newPassword = formGroup.get(newPasswordKey)?.value;
      return oldPassword !== newPassword ? null : { sameAsOldPassword: 'Mật khẩu mới không được trùng với mật khẩu cũ.' };
    };
  }
}
