import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  standalone: true,
  name: 'customCurrency'
})
export class CustomCurrencyPipe implements PipeTransform {
  transform(value: number, currencyCode: string = '₫', symbolDisplay: boolean = true, digitsInfo: string = '1.0-0'): string {
    if (value == null) return '';

    const formattedValue = new Intl.NumberFormat('vi-VN', {
      style: 'currency',
      currency: 'VND',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0
    }).format(value);

    // Move the currency symbol to the end
    return `${formattedValue.replace('₫', '')} ${currencyCode}`;
  }
}
