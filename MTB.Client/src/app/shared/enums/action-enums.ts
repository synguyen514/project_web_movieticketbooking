﻿export enum ActionEnum {
  Detail = 1,
  Create = 2,
  Edit = 3,
  Delete = 4,
  Approve = 5,
  Reject = 6,
}
