export enum Role {
  Admin = 1,
  Manager = 2,
  Staff = 3,
  User = 4
}
