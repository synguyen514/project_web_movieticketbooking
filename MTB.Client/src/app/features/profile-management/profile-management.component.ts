import {Component, OnInit} from '@angular/core';
import {CurrencyPipe, DatePipe, NgForOf} from "@angular/common";
import {NavBarComponent} from "../../shared/layouts/nav-bar/nav-bar.component";
import {NzTableComponent, NzThMeasureDirective} from "ng-zorro-antd/table";
import {ITicket} from "../../models/ticket-models";
import {AuthService} from "../../services/authentication-service";
import {TicketService} from "../../services/ticket-service";
import {IUser, IUserUpdateImage} from "../../models/user-model";
import {NzImageDirective, NzImageModule} from "ng-zorro-antd/image";
import {NzUploadChangeParam, NzUploadComponent, NzUploadFile} from "ng-zorro-antd/upload";
import {NzButtonComponent} from "ng-zorro-antd/button";
import {NzIconDirective} from "ng-zorro-antd/icon";
import {environment} from "ng-zorro-antd/core/environments";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {FileUploadService} from "../../services/file-upload-service";
import {ImageService} from "../../shared/services/path-image.service";
import {UserService} from "../../services/user-service";

@Component({
  selector: 'app-profile-management',
  standalone: true,
  imports: [
    CurrencyPipe,
    DatePipe,
    NavBarComponent,
    NgForOf,
    NzTableComponent,
    NzThMeasureDirective,
    NzImageDirective,
    NzUploadComponent,
    NzButtonComponent,
    NzIconDirective,
    NzImageModule
  ],
  templateUrl: './profile-management.component.html',
  styleUrl: './profile-management.component.scss'
})
export class ProfileManagementComponent implements OnInit {
  // @ts-ignore
  userData: IUser = this.authService.getUserData();
  uploadUrl = 'https://localhost:5016/api/file-management/upload';
  imageUrl: string = '';
  imageName: string | undefined = this.authService.getUserData()?.imageName;
  // @ts-ignore
  fileList: NzUploadFile[] =[];
  // @ts-ignore
  userDataUpdate: IUserUpdateImage;
  constructor(
    private authService: AuthService,
    private notification: NzNotificationService,
    private fileUploadService: FileUploadService,
    private imageService: ImageService,
    private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this.imageUrl = this.imageService.getImageUrl(this.imageName);

    console.log(this.imageUrl);
  }

  handleChange(info: NzUploadChangeParam): void {
    if (info.file.status === 'done') {
      console.log(`${info.file.name} file uploaded successfully.`);
      this.imageName = info.file.name;
      this.fileList = info.fileList;
    } if (info.file.status === 'removed') {
      this.fileList = [];
    }
    // @ts-ignore
    this.imageUrl = this.imageService.getImageUrl(this.imageName);
  }

  uploadFiles(): void {
    // Gọi service để upload file
    if (this.fileList.length === 0) {
      this.notification.warning('Cảnh báo', 'Vui lòng chọn ít nhất một tệp để tải lên.');
      return;
    }
    const filesToUpload: File[] = this.fileList.map(file => file.originFileObj as File);
    this.fileUploadService.uploadFiles(this.authService.getAccessToken(), filesToUpload[0]).subscribe(
      response => {
        console.log('Upload successful!', response);
        this.notification.success('Thành công', 'Tải lên thành công.');
        this.userDataUpdate = {
          id: <number>this.authService.getUserData()?.id,
          imageId: parseInt(response.imageId)
        }
        this.userService.updateImageUser(this.userDataUpdate, this.authService.getAccessToken()).subscribe({
          next: (response) => {
            if (response.isSuccess) {
              this.notification.success('Thành công', 'Cập nhật ảnh thành công.');
              this.authService.updateUserProperty('imageName', this.imageName)
            }
          },
          error: (err) => {
            this.notification.error('Lỗi', 'Đã xảy ra lỗi khi tải lên.');
          }
        });
      },
      error => {
        console.error('Upload error:', error);
        this.notification.error('Lỗi', 'Đã xảy ra lỗi khi tải lên.');
      }
    );
  }
}
