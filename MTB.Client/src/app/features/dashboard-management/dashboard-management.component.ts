import {Component, OnInit} from '@angular/core';
import {NzIconDirective} from "ng-zorro-antd/icon";
import {ITicket} from "../../models/ticket-models";
import {AuthService} from "../../services/authentication-service";
import {TicketService} from "../../services/ticket-service";
import {CurrencyPipe, DatePipe, NgForOf} from "@angular/common";
import {NzTableComponent, NzThMeasureDirective} from "ng-zorro-antd/table";
import {tick} from "@angular/core/testing";

@Component({
  selector: 'app-dashboard-management',
  standalone: true,
  imports: [
    NzIconDirective,
    CurrencyPipe,
    DatePipe,
    NgForOf,
    NzTableComponent,
    NzThMeasureDirective
  ],
  templateUrl: './dashboard-management.component.html',
  styleUrl: './dashboard-management.component.scss'
})
export class DashboardManagementComponent implements OnInit {
  tickets: ITicket[] = [];
  sumTicketPrice: number = 0;

  constructor(
    private authService: AuthService,
    private ticketService: TicketService,
  ) {
  }

  ngOnInit(): void {
    this.getTicketsData(this.authService.getAccessToken());
  }

  public getTicketsData(token: string) {
    this.ticketService.getAllTickets(token).subscribe(
      (data) => {
        this.tickets = data;
        this.tickets.forEach(ticket => {
          this.sumTicketPrice = this.sumTicketPrice + ticket.price;
        })
      },
      (error) => {
        console.log(error);
      }
    );
  }

  handleLogout(): void {
    // @ts-ignore
    this.authService.logout(this.authService.getUserData().id);
  }
}
