import {Component, OnInit} from '@angular/core';
import {MovieCardComponent} from "../components/movie-card/movie-card.component";
import {NavBarComponent} from "../../shared/layouts/nav-bar/nav-bar.component";
import {CurrencyPipe, DatePipe, NgForOf} from "@angular/common";
import {NzTableComponent, NzThMeasureDirective} from "ng-zorro-antd/table";
import {ITicket} from "../../models/ticket-models";
import {AuthService} from "../../services/authentication-service";
import {TicketService} from "../../services/ticket-service";

@Component({
  selector: 'app-list-ticket-booking',
  standalone: true,
  imports: [
    MovieCardComponent,
    NavBarComponent,
    NgForOf,
    NzTableComponent,
    NzThMeasureDirective,
    CurrencyPipe,
    DatePipe
  ],
  templateUrl: './list-ticket-booking.component.html',
  styleUrl: './list-ticket-booking.component.scss'
})
export class ListTicketBookingComponent implements OnInit {
  tickets: ITicket[] = [];

  constructor(
    private authService: AuthService,
    private ticketService: TicketService,
  ) {
  }

  ngOnInit(): void {
    this.getTicketsData(this.authService.getUserData()?.id.toString(), this.authService.getAccessToken());
  }

  public getTicketsData(id: string | undefined, token: string) {
    this.ticketService.getTicketByUserId(id, token).subscribe(
      (data) => {
        this.tickets = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
