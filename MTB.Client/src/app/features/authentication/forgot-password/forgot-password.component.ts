import {Component} from '@angular/core';
import {FormsModule, NonNullableFormBuilder, ReactiveFormsModule, Validators} from "@angular/forms";
import {NgIf} from "@angular/common";
import {NzIconDirective} from "ng-zorro-antd/icon";
import {SpinnerComponent} from "../../../shared/components/action-model/SpinnerComponent";
import {SvgIconComponent} from "../../../shared/components/svg-icon/svg-icon.component";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {AuthService} from "../../../services/authentication-service";
import {Title} from "@angular/platform-browser";
import {Router} from "@angular/router";
import {IUserUpdatePassWord} from "../../../models/user-model";
import {UserService} from "../../../services/user-service";
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-forgot-password',
  standalone: true,
  imports: [
    FormsModule,
    NgIf,
    NzIconDirective,
    ReactiveFormsModule,
    SpinnerComponent,
    SvgIconComponent
  ],
  templateUrl: './forgot-password.component.html',
  styleUrl: './forgot-password.component.scss'
})
export class ForgotPasswordComponent {
// @ts-ignore
  formForGot: FormGroup;
  showPassword = false;
  isLoading = false;
  // @ts-ignore
  userUpdateData: IUserUpdatePassWord;

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private notification: NzNotificationService,
    private authService: AuthService,
    private userService: UserService,
    private readonly titleService: Title,
    private router: Router
  ) {
    this.titleService.setTitle(`MTB`);
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/home-page']);
    }

    this.formForGot = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
    })
  }

  onSubmit() {
    this.isLoading = true;
    if (!this.formForGot.valid) {
      this.notification.error('Error', 'Email không hợp lệ');
      console.log(this.formForGot.value);
      this.isLoading = false;
      return;
    }

    const email = this.formForGot.value.email;
    this.authService.sendOTPForMail(email).subscribe({
      next: (response) => {
        this.isLoading = false;
        if (response.isSuccess) {
          this.userUpdateData = {
            email: email,
            password: (CryptoJS.MD5(response.defaultPassword)).toString().toUpperCase()
          }
          this.userService.updateUser(this.userUpdateData, this.authService.getAccessToken()).subscribe({
            next: (response) => {
              this.isLoading = false;
              if (response.isSuccess) {
                this.notification.success('Đặt lại mật khẩu mới thành công ','');
              }
            },
            error: (err) => {
              this.isLoading = false;
              this.notification.error('Đặt lại mật khẩu mới không thành công',
                err.error.message);
            }
          });
        }
      },
      error: (err) => {
        this.isLoading = false;
        this.notification.error('Đặt lại mật khẩu mới không thành công',
          err.error.message);
      }
    });
  }

  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }
}
