import {Route} from '@angular/router';
import {ChangePasswordComponent} from "./change-password/change-password.component";

export const routes: Route[] = [
  {
    path: 'auth',
    loadComponent: () =>
      import('./authentication.component').then((c) => c.AuthenticationComponent),
  },
  {
    path: 'sign-in',
    loadComponent: () => import('./sign-in/sign-in.component').then(c => c.SignInComponent)
  },
  {
    path: 'sign-up',
    loadComponent: () => import('./sign-up/sign-up.component').then(c => c.SignUpComponent)
  },
  {
    path: 'change-password',
    loadComponent: () => import('./change-password/change-password.component').then(c => c.ChangePasswordComponent)
  },
  {
    path: 'forgot-password',
    loadComponent: () => import('./forgot-password/forgot-password.component').then(c => c.ForgotPasswordComponent)
  }
];

