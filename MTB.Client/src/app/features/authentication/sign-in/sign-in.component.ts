import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormGroup, FormsModule, NonNullableFormBuilder, ReactiveFormsModule, Validators} from "@angular/forms";
import {SvgIconComponent} from "../../../shared/components/svg-icon/svg-icon.component";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {AuthService} from "../../../services/authentication-service";
//import {RoleService} from "../../../services/role-service";
import {NzIconDirective} from "ng-zorro-antd/icon";
import {SpinnerComponent} from "../../../shared/components/action-model/SpinnerComponent";
import {NzSpinComponent} from "ng-zorro-antd/spin";
@Component({
  selector: 'app-sign-in',
  standalone: true,
  templateUrl: './sign-in.component.html',
  imports: [
    CommonModule,
    FormsModule,
    SvgIconComponent,
    ReactiveFormsModule,
    NzIconDirective,
    SpinnerComponent,
    NzSpinComponent,
  ],
})
export class SignInComponent {

  // @ts-ignore
  form: FormGroup;
  showPassword = false;
  isLoading = false;

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private notification: NzNotificationService,
    private authService: AuthService,
    //private roleService: RoleService,
    private readonly titleService: Title,
    private router: Router
  ) {
    this.titleService.setTitle(`MTB`);
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/home-page']);
    }

    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      rememberMe: [false]
    })
  }

  onSubmit() {
    this.isLoading = true;
    if (!this.form.valid) {
      this.notification.error('Error', 'Email hoặc mật khẩu không hợp lệ');
      console.log(this.form.value);
      this.isLoading = false;
      return;
    }

    const { email, password } = this.form.value;
    this.authService.login({ email, password }).subscribe({
      next: (response) => {
        const userInfo = response.user;
        this.authService.mapDataLogin(response);
        this.authService.currentUser.next(userInfo);
        this.isLoading = false;
        this.notification.success('Đăng nhập thành công tài khoản ',
          `${userInfo.email}`);
        this.authService.redirectToUrl(`/home-page`);
      },
      error: (err) => {
        this.isLoading = false;
        this.notification.error('Đăng nhập không thành công',
          err.error.message);
        this.authService.redirectToLogin();
      }
    });
  }

  togglePasswordVisibility() {
    this.showPassword = !this.showPassword;
  }

}
