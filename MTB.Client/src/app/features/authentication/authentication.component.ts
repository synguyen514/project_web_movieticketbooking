import {Component, OnInit, NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {NzInputDirective} from "ng-zorro-antd/input";
import {NzButtonModule} from "ng-zorro-antd/button";
import {SvgIconComponent} from "../../shared/components/svg-icon/svg-icon.component";
import {RouterOutlet} from "@angular/router";


@Component({
  selector: 'app-test-page',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    NzInputDirective,
    NzButtonModule,
    SvgIconComponent,
    RouterOutlet,
  ],
  templateUrl: './authentication.component.html',
  styleUrl: './authentication.component.scss'
})
export class AuthenticationComponent implements OnInit {
  message: string = "";

  constructor(

  ) {

  }

  ngOnInit(): void {

  }

}
