import { Route } from '@angular/router';
import {HomePageManagementComponent} from "./home-page-management.component";

export const routes: Route[] = [
  {
    path: '',
    loadComponent: () =>
      import('./home-page-management.component').then((c) => c.HomePageManagementComponent),
  }
];

