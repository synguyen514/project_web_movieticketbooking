import {Component, OnInit} from '@angular/core';
import {MovieCardComponent} from "../components/movie-card/movie-card.component";
import {NgForOf, NgIf} from "@angular/common";
import {NavBarComponent} from "../../shared/layouts/nav-bar/nav-bar.component";
import {NonNullableFormBuilder, Validators} from "@angular/forms";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {AuthService} from "../../services/authentication-service";
import {DomSanitizer, SafeUrl, Title} from "@angular/platform-browser";
import {Router} from "@angular/router";
import {MoviesService} from "../../services/movies-service";
import {IMovie} from "../../models/movie-model";
import {IGenre, IGenreMovie} from "../../models/genre-models";
import {GenreService} from "../../services/genre-service";
import {VnpayService} from "../../services/payment-service";
import {NzStepComponent, NzStepsComponent} from "ng-zorro-antd/steps";

@Component({
  selector: 'app-home-page-management',
  standalone: true,
  imports: [
    MovieCardComponent,
    NgForOf,
    NavBarComponent,
    NgIf,
    NzStepsComponent,
    NzStepComponent
  ],
  templateUrl: './home-page-management.component.html',
  styleUrl: './home-page-management.component.scss'
})
export class HomePageManagementComponent implements OnInit {

  accessToken = this.authService.getAccessToken();

  genres: IGenre[] = [];
  movies: IMovie[] = [];
  movieByGenre: IGenreMovie[] = [];
  // @ts-ignore
  paymentUrl: SafeUrl;

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private notification: NzNotificationService,
    private authService: AuthService,
    private movieService: MoviesService,
    private genreService: GenreService,
  ) {

  }
  ngOnInit(): void {
    this.getMoviesData(this.accessToken);
    this.getGenreData(this.accessToken);
  }

  getMoviesData(token: string) {
    this.movieService.getAllMovies(token).subscribe(
      (data) => {
        this.movies = data;
        console.log(this.movies);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getMoviesDataByGenre(genreData: IGenre) {
    this.movieService.getAllMoviesByGenre(this.accessToken, genreData.id.toString()).subscribe(
      (data) => {
        this.movieByGenre.push({ name: genreData.genreName, movies: data });
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getGenreData(token: string) {
    this.genreService.getAllGenre(token).subscribe(
      (data) => {
        this.genres = data;
        this.getInitData()
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getInitData() {
    this.genres.forEach((genre) => {
      this.getMoviesDataByGenre(genre);
    });
  }

}
