import {Component, Input, OnInit} from '@angular/core';
import {ConfirmModalComponent} from "../../../shared/components/confirm-modal/confirm-modal.component";
import {DatePipe} from "@angular/common";
import {IMovie} from "../../../models/movie-model";

@Component({
    selector: 'app-movie-card',
    templateUrl: './movie-card.component.html',
    standalone: true,
  imports: [
    ConfirmModalComponent,
    DatePipe
  ],
    styleUrls: ['./movie-card.component.scss']
})
export class MovieCardComponent implements OnInit{
  // @ts-ignore
  @Input() movie: IMovie;
  isVisibleMiddle = false;

  constructor() {}

  ngOnInit(): void {}
  getCurrentDateTime(): string {
    const now = new Date();
    return now.toISOString().replace(/-|:|\.\d+/g, '');
  }
  showModal(): void {
    this.isVisibleMiddle = true;
  }

  onCancelModal() {
    this.isVisibleMiddle = false;
  }

  onConfirm() {

  }
}
