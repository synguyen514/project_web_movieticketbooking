import {ApplicationConfig, importProvidersFrom, provideZoneChangeDetection} from '@angular/core';
import { provideRouter } from '@angular/router';
import {appRoutes} from './app.routes';
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {en_US, provideNzI18n} from "ng-zorro-antd/i18n";

export const appConfig: ApplicationConfig = {
  providers: [
    provideZoneChangeDetection({ eventCoalescing: true }),
    provideRouter(appRoutes),
    provideNzI18n(en_US),
    importProvidersFrom(BrowserAnimationsModule),
    provideHttpClient(
      withInterceptorsFromDi()
    ),
  ],
};
