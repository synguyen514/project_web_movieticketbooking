import {Route} from '@angular/router';
import {PageNotFoundComponent} from "./shared/layouts/page-notfound/page-notfound.component";
import {AuthenticationComponent} from "./features/authentication/authentication.component";
import {HomePageManagementComponent} from "./features/home-page-management/home-page-management.component";
import {ListTicketBookingComponent} from "./features/list-ticket-booking/list-ticket-booking.component";
import {DashboardManagementComponent} from "./features/dashboard-management/dashboard-management.component";
import {ProfileManagementComponent} from "./features/profile-management/profile-management.component";

export const appRoutes: Route[] = [
  {
    path: '',
    component: HomePageManagementComponent,
    loadChildren: () =>
      import('./features/home-page-management/home-page-management.routes').then((r) => r.routes),
  },
  {
    path: 'home-page',
    component: HomePageManagementComponent,
    loadChildren: () =>
      import('./features/home-page-management/home-page-management.routes').then((r) => r.routes),
  },
  {
    path: 'notfound',
    component: PageNotFoundComponent,
    loadChildren: () =>
      import('./shared/layouts/page-notfound/page-notfound.routes').then((r) => r.routes),
  },
  {
    path: 'auth',
    component: AuthenticationComponent,
    loadChildren: () =>
      import('./features/authentication/authentication.routes').then((r) => r.routes),
  },
  {
    path: 'list-ticket-booking',
    loadComponent: () => import('./features/list-ticket-booking/list-ticket-booking.component').then(c => c.ListTicketBookingComponent)
  },
  {
    path: 'dashboard',
    loadComponent: () => import('./features/dashboard-management/dashboard-management.component').then(c => c.DashboardManagementComponent)
  },
  {
    path: 'profile',
    loadComponent: () => import('./features/profile-management/profile-management.component').then(c => c.ProfileManagementComponent)
  },
];
