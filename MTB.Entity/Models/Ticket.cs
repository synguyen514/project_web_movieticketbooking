﻿using System;
using System.Collections.Generic;

namespace MTB.Entity.Models
{
    public partial class Ticket
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? MovieId { get; set; }
        public int? CinemaId { get; set; }
        public DateTime? DateOrder { get; set; }
        public double? Price { get; set; }
        public int? SeatNumber { get; set; }
        public int? HallNumber { get; set; }
        public string? Status { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual Cinema? Cinema { get; set; }
        public virtual Movie? Movie { get; set; }
        public virtual User? User { get; set; }
    }
}
