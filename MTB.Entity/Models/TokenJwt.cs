﻿using System;
using System.Collections.Generic;

namespace MTB.Entity.Models
{
    public partial class TokenJwt
    {
        public int Id { get; set; }
        public string JwtId { get; set; } = null!;
        public int UserId { get; set; }
        public string Token { get; set; } = null!;
        public bool? IsUsed { get; set; }
        public bool? IsRevoked { get; set; }
        public DateTime? IssueAt { get; set; }
        public DateTime? ExpireAt { get; set; }

        public virtual User User { get; set; } = null!;
    }
}
