﻿using System;
using System.Collections.Generic;

namespace MTB.Entity.Models
{
    public partial class User
    {
        public User()
        {
            Tickets = new HashSet<Ticket>();
            TokenJwts = new HashSet<TokenJwt>();
        }

        public int Id { get; set; }
        public string? FullName { get; set; }
        public bool? Gender { get; set; }
        public DateTime? Dob { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? Password { get; set; }
        public int? RoleId { get; set; }
        public int? ImageId { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual Image? Image { get; set; }
        public virtual Role? Role { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
        public virtual ICollection<TokenJwt> TokenJwts { get; set; }
    }
}
