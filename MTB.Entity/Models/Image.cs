﻿using System;
using System.Collections.Generic;

namespace MTB.Entity.Models
{
    public partial class Image
    {
        public Image()
        {
            Movies = new HashSet<Movie>();
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string? ImageName { get; set; }
        public DateTime? CreateDate { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
