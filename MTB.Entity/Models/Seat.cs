﻿using System;
using System.Collections.Generic;

namespace MTB.Entity.Models
{
    public partial class Seat
    {
        public int Id { get; set; }
        public int? HallId { get; set; }
        public string? SeatCode { get; set; }
        public bool? IsBookingActive { get; set; }
        public bool? IsDeleted { get; set; }

        public virtual Hall? Hall { get; set; }
    }
}
