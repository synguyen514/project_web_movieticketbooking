namespace MTB.Entity.DTOs.MovieDTO;

public class MovieDTO
{
    public int Id { get; set; }
    public string? MovieName { get; set; }
    public string? YearOfManufacture { get; set; }
    public DateTime? ReleaseDate { get; set; }
    public string? Description { get; set; }
    public int? ImageId { get; set; }
    public string? ImageNameUrl { get; set; }
    public string? TrailerUrl { get; set; }
    public int? GenreId { get; set; }
    public int? CountryId { get; set; }
    public bool? IsDeleted { get; set; }
}