namespace MTB.Entity.DTOs.MovieDTO;

public class GenreDTO
{
    public int Id { get; set; }
    public string? GenreName { get; set; }
    public bool? IsDeleted { get; set; }
}