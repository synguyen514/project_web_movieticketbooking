namespace MTB.Entity.DTOs;

public class CinemaDTO
{
    public int Id { get; set; }
    public string? CinemaName { get; set; }
    public string? Address { get; set; }
    public bool? IsDeleted { get; set; }
}