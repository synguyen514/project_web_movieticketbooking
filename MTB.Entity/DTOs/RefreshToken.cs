﻿using MTB.Entity.Models;

namespace MTB.Entity.DTOs;

public class RefreshToken
{
    public string TokenId { get; set; } = default!;
    
    public Guid UserId { get; set; }
    
    public string Token { get; set; } = null!;
    
    public DateTime Created { get; set; }
    
    public DateTime ExpiredAt { get; set; }
    
    public bool? IsRevoked { get; set; }
    
    public string JwtId { get; set; } = default!;

    public virtual User User { get; set; } = default!;
}