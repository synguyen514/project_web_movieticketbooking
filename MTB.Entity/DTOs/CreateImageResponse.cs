﻿using WMS.Entity.DTOs;

public class CreateImageResponse : BaseResponseDto
{
    public string ImageId { get; set; }
}