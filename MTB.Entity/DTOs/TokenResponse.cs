﻿using MTB.Entity.DTOs.UserDTO;
using WMS.Entity.DTOs.UserDTO;

namespace MTB.Entity.DTOs;

public class TokenResponse
{
    public string AccessToken { get; set; } = default!;
    public string RefreshToken { get; set; } = default!;
    public UserAuthDto User { get; set; } = default!;
}