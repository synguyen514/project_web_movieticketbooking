﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Entity.DTOs.UserDTO
{
    public class UserAuthDto : UserDto
    {
        public string? ImageName { get; set; }

        public string? RoleName { get; set; }
    }
}
