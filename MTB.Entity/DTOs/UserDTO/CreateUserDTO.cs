﻿namespace WMS.Entity.DTOs.UserDTO
{
    public class CreateUserDto
    {
        public string FullName { get; set; } = null!;

        public DateOnly? Dob { get; set; }

        public bool? Gender { get; set; }

        public string? Address { get; set; }

        public string? Phone { get; set; }

        public string Email { get; set; } = null!;

        public string Password { get; set; } = null!;

        public Guid DepartmentId { get; set; }

        public Guid ImageId { get; set; }

        public Guid RoleId { get; set; }
    }
}
