﻿namespace WMS.Entity.DTOs.UserDTO
{
    public class UserDto
    {
        public int Id { get; set; }

        public string? FullName { get; set; } = null!;

        public DateTime? Dob { get; set; }

        public bool? Gender { get; set; }

        public string? Address { get; set; }

        public string? Phone { get; set; }

        public string? Email { get; set; } = null!;

        public string? Password { get; set; } = null!;

        public int? RoleId { get; set; }

    }
}
