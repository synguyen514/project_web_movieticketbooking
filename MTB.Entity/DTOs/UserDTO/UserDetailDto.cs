﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTB.Entity.Models;

namespace MTB.Entity.DTOs.UserDTO
{
    public class UserDetailDto
    {
        public int Id { get; set; }
        public string? FullName { get; set; }
        public bool? Gender { get; set; }
        public DateTime? Dob { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? Address { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? Password { get; set; }
        public int? RoleId { get; set; }
        public int? ImageId { get; set; }
        public bool? IsDeleted { get; set; }
    }
}