namespace MTB.Entity.DTOs.UserDTO;

public class UpdateUserImage
{
    public int Id { get; set; }

    public int? ImageId { get; set; }
}