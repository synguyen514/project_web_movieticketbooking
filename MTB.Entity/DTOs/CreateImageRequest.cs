namespace MTB.Entity.DTOs;

public class CreateImageRequest
{
    public string? ImageName { get; set; }
}