namespace MTB.Entity.DTOs;

public class ImageDTO
{
    public string? ImageName { get; set; }
}