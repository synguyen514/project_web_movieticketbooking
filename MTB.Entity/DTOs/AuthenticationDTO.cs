﻿using System.ComponentModel.DataAnnotations;

namespace MTB.Entity.DTOs;

public class AuthenticationDTO
{
    [Required] public string Email { get; set; } = default!;

    [Required] public string Password { get; set; } = default!;
}