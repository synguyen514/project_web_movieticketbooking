﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WMS.Entity.DTOs
{
    public class OTPResponse : BaseResponseDto
    {
        public string OTP{get;set;}
        public string DefaultPassword { get;set;}

    }
}
