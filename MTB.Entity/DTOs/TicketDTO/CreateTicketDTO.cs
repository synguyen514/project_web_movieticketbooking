namespace MTB.Entity.DTOs;

public class CreateTicketDTO
{
    public int? UserId { get; set; }
    public int? MovieId { get; set; }
    public int? CinemaId { get; set; }
    public int? SeatNumber { get; set; }
}