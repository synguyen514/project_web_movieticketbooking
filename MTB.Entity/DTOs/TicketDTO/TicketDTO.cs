namespace MTB.Entity.DTOs;

public class TicketDTO
{
    public int Id { get; set; }
    public int? UserId { get; set; }
    public int? MovieId { get; set; }
    public string? MovieName { get; set; }
    public int? CinemaId { get; set; }
    public string? CinemaName { get; set; }
    public DateTime? DateOrder { get; set; }
    public double? Price { get; set; }
    public int? SeatNumber { get; set; }
    public int? HallNumber { get; set; }
    public string? Status { get; set; }
    public bool? IsDeleted { get; set; }
}