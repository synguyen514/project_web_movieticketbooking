CREATE DATABASE [Movie_Ticket_Booking]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Movie_Ticket_Booking', FILENAME = N'/var/opt/mssql/data/Movie_Ticket_Booking.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Movie_Ticket_Booking_log', FILENAME = N'/var/opt/mssql/data/Movie_Ticket_Booking_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 COLLATE SQL_Latin1_General_CP1_CI_AS
GO
ALTER DATABASE [Movie_Ticket_Booking] SET COMPATIBILITY_LEVEL = 150
GO
ALTER DATABASE [Movie_Ticket_Booking] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET ARITHABORT OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET AUTO_CREATE_STATISTICS OFF
GO
ALTER DATABASE [Movie_Ticket_Booking] SET AUTO_UPDATE_STATISTICS OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET  READ_WRITE 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET  MULTI_USER 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [Movie_Ticket_Booking] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Movie_Ticket_Booking] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Movie_Ticket_Booking]
GO
IF NOT EXISTS (SELECT name FROM sys.filegroups WHERE is_default=1 AND name = N'PRIMARY') ALTER DATABASE [Movie_Ticket_Booking] MODIFY FILEGROUP [PRIMARY] DEFAULT
GO
create table [dbo].[User](
id INT PRIMARY KEY IDENTITY(1,1),
full_name nvarchar(100),
gender bit,
dob date,
email varchar(100),
phone varchar(10),
address nvarchar(100),
create_date date,
[password] varchar(100),
role_id int,
is_deleted bit,
CONSTRAINT fk_role_user FOREIGN KEY (role_id) REFERENCES Role (id)
)
GO
create table dbo.TokenJwt
(
id int not null primary key IDENTITY(1,1),
jwt_id varchar(max) not null,
[user_id] int not null,
token varchar(max) not null,
is_used bit,
is_revoked bit,
issue_at datetime,
expire_at datetime
CONSTRAINT fk_booking_user FOREIGN KEY ([user_id]) REFERENCES [User] (id)
)
GO
create table [dbo].[Role](
id INT PRIMARY KEY IDENTITY(1,1),
role_name nvarchar(100),
is_deleted bit
)
GO
create table [dbo].[Country](
id INT PRIMARY KEY IDENTITY(1,1),
country_name nvarchar(100),
is_deleted bit
)
GO
create table [dbo].[Genres](
id INT PRIMARY KEY IDENTITY(1,1),
genre_name nvarchar(100),
is_deleted bit
)
GO
create table [dbo].[Image](
id INT PRIMARY KEY IDENTITY(1,1),
image_name nvarchar(100),
create_date date,
is_deleted bit
)
GO
create table [dbo].[Movies](
id INT PRIMARY KEY IDENTITY(1,1),
movie_name nvarchar(100),
year_of_manufacture varchar(100),--Nam san xuat
release_date datetime, --ngay phat hanh
[description] nvarchar(MAX),
image_id int, --link anh or poster
genre_id int, --the loai
country_id int, --quoc gia
CONSTRAINT fk_image_movie FOREIGN KEY (image_id) REFERENCES [Image] (id),
CONSTRAINT fk_genre_movie FOREIGN KEY (genre_id) REFERENCES [Genres] (id),
CONSTRAINT fk_country_movie FOREIGN KEY (country_id) REFERENCES Country (id)
)
GO
create table [dbo].[Cinemas](
id INT PRIMARY KEY IDENTITY(1,1),
cinema_name nvarchar(100),
address nvarchar(100),
is_deleted bit
)
GO
create table [dbo].[Halls](
id INT PRIMARY KEY IDENTITY(1,1),
cinema_id int,
hall_name nvarchar(100),
number_seat int,
is_deleted bit,
CONSTRAINT fk_hall_cinema FOREIGN KEY (cinema_id) REFERENCES Cinemas (id)
)
GO
create table [dbo].[Seats](
id INT PRIMARY KEY IDENTITY(1,1),
hall_id int,
seat_code varchar(100),
is_deleted bit,
CONSTRAINT fk_seat_hall FOREIGN KEY (hall_id) REFERENCES Halls (id)
)
GO
create table [dbo].[TimeShows]( --XUAT CHIEU
id INT PRIMARY KEY IDENTITY(1,1),
movie_id int,
cinema_id int,
hall_id int,
show_date date,
start_time time,
end_time time,
is_deleted bit,
CONSTRAINT fk_movie_show FOREIGN KEY (movie_id) REFERENCES Movies (id),
CONSTRAINT fk_hall_show FOREIGN KEY (hall_id) REFERENCES Halls (id),
CONSTRAINT fk_cinema_show FOREIGN KEY (cinema_id) REFERENCES Cinemas (id)
)
GO
create table [dbo].[Tickets](
id INT PRIMARY KEY IDENTITY(1,1),
sell_date date, --ngay ban
price float,
seat_id int,
timeshow_id int,
[status] int,
is_deleted bit,
CONSTRAINT fk_ticket_seat FOREIGN KEY (seat_id) REFERENCES Seats (id),
CONSTRAINT fk_ticket_show FOREIGN KEY (timeshow_id) REFERENCES TimeShows (id)
)
GO
create table [dbo].[Bookings](
ticket_id int,
[user_id] int,
is_deleted bit,
CONSTRAINT pk_booking PRIMARY KEY (ticket_id, [user_id]),
CONSTRAINT fk_booking_ticket FOREIGN KEY (ticket_id) REFERENCES Tickets (id),
CONSTRAINT fk_booking_user FOREIGN KEY ([user_id]) REFERENCES [User] (id)
)
GO