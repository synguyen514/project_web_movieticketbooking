﻿using System.Net;
using System.Net.Mail;
using WMS.Shared.Constants;

namespace WMS.Shared.Extensions;


    public interface IMailSmtpExtension
    {
        Task Send(string toMail, string pass, string title, string toName);
        string GetOTP();
        string GeneratePassword();
    }

    public class MailSmtpExtension : IMailSmtpExtension
{      
        public async Task Send(string toMail, string pass, string title, string toName)
        {
            try
            { 
                //var frommail = new MailAddress(MailConstant.HostMail);
               MailMessage mm = new MailMessage(MailConstant.HostMail, toMail);
                mm.Subject = title;
                mm.Body = "<!DOCTYPE html>\r\n<html lang=\"en\">\r\n<head>\r\n" +
                    "    <meta charset=\"UTF-8\"/>\r\n    <meta name=\"viewport\"" +
                    " content=\"width=device-width, initial-scale=1.0\"/>\r\n" +
                    "    <title>Reset password</title>\r\n" +
                    "    <style>\r\n        body {\r\n            font-family: Arial, sans-serif;\r\n" +
                    "            margin: 0;\r\n            padding: 0;\r\n            background-color: #f4f4f4;\r\n" +
                    "        }\r\n\r\n        .container {\r\n            max-width: 600px;\r\n            margin: 0 auto;\r\n" +
                    "            padding: 20px;\r\n            background-color: #ffffff;\r\n            border-radius: 10px;\r\n" +
                    "            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);\r\n        }\r\n\r\n        h2 {\r\n            color: #333;\r\n" +
                    "        }\r\n\r\n        p {\r\n            color: #555;\r\n        }\r\n\r\n        .button {\r\n" +
                    "            display: inline-block;\r\n            padding: 10px 20px;\r\n            text-decoration: none;\r\n" +
                    "            background-color: #0061C1;\r\n            color: #ffffff !important;\r\n            font-weight: bold;\r\n" +
                    "            border-radius: 5px;\r\n        }\r\n    </style>\r\n</head>\r\n<body>\r\n<div class=\"container\">\r\n" +
                    "    <p>Gửi <strong>"+toName+", </strong></p>\r\n       <p>\r\n" +
                    "        Chúng tôi là đội ngũ hỗ trợ vận hành hệ thống đặt vé xem phim gửi cho bạn email này để xác nhận việc đặt lại mật mới cũ của bạn.</p>\r\n" +
                    "        <p>Mật khẩu mặc định của bạn sau khi đặt lại là: <b>" + pass + "</b></p>\r\n" +
                    "        </div>\r\n</body>\r\n</html>\r\n";

            mm.IsBodyHtml = true;
            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            var networkCred = new NetworkCredential
            {
                UserName = MailConstant.HostMail,
                Password = MailConstant.AppPassword
            };
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = networkCred;
            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.Send(mm);
        }
        catch (Exception e)
        {
            throw new Exception(e.Message);
        }
    }

        public string GetOTP()
        {

            string UpperCase = "1234567890";
            string LowerCase = "1234567890";
            string Digits = "1234567890";
            string allCharacters = UpperCase + LowerCase + Digits;
            //Random will give random charactors for given length  
            Random r = new Random();
            String password = "";
            for (int i = 0; i < 6; i++)
            {
                double rand = r.NextDouble();
                if (i == 0)
                {
                    password += UpperCase.ToCharArray()[(int)Math.Floor(rand * UpperCase.Length)];
                }
                else
                {
                    password += allCharacters.ToCharArray()[(int)Math.Floor(rand * allCharacters.Length)];
                }
            }
            return password;
        }

        public string GeneratePassword()
        {
            string uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string lowercase = "abcdefghijklmnopqrstuvwxyz";
            string digits = "0123456789";
            string specialChars = "!@#$%^&*()_+~`|}{[]:;?><,./-=";

            var password = new System.Text.StringBuilder();

            password.Append(GetRandomChar(uppercase));
            password.Append(GetRandomChar(lowercase));
            password.Append(GetRandomChar(digits));
            password.Append(GetRandomChar(specialChars));

            for (int i = 4; i < 8; i++)
            {
                string charSet = uppercase + lowercase + digits + specialChars;
                password.Append(GetRandomChar(charSet));
            }

            return new string(password.ToString().OrderBy(x => Guid.NewGuid()).ToArray());
        }

        private static char GetRandomChar(string charSet)
        {
            Random random = new Random();
            return charSet[random.Next(0, charSet.Length)];
        }
    }

