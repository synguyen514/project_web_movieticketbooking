using Microsoft.AspNetCore.Mvc;
using MTB.Entity.DTOs.UserDTO;
using MTB.Services;
using WMS.Entity.DTOs.UserDTO;

namespace MTB.APIs.Controllers;

//[Authorize]
[Route("api/user-management")]
[ApiController]

public class UserController : ControllerBase
{
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
        _userService = userService;
    }
    
    [HttpGet("get-all")]
    public async Task<IActionResult> GetAllMovies()
    { 
        return Ok();
    }
    
    [HttpPut("update-password")]
    public async Task<IActionResult> UpdateUser(UpdateUserDto request)
    {
        var response = await _userService.UpdatePassUser(request);
        if (response.IsSuccess)
            return Ok(response);
        return BadRequest(response);
    }
    
    [HttpPut("update-image")]
    public async Task<IActionResult> UpdateImageUser(UpdateUserImage request)
    {
        var response = await _userService.UpdateImageUser(request);
        if (response.IsSuccess)
            return Ok(response);
        return BadRequest(response);
    }
}