using Microsoft.AspNetCore.Mvc;
using MTB.Entity.DTOs;
using MTB.Services;

namespace MTB.APIs.Controllers;

//[Authorize]
[Route("api/ticket-management")]
[ApiController]
public class TicketController : ControllerBase
{
    private readonly ITicketsService _ticketService;

    public TicketController(ITicketsService ticketService)
    {
        _ticketService = ticketService;
    }
    
    [HttpGet("get-all")]
    public async Task<IActionResult> GetAllMovies()
    {
        var genres = await _ticketService.GetAllTickets();
        if (genres is not null && genres.Count > 0)
            return Ok(genres);
        return BadRequest("Dont have ticket in system");
    }
    
    [HttpGet("get-all-by-userId/{id}")]
    public async Task<IActionResult> GetAllMovies(string id)
    {
        var genres = await _ticketService.GetAllTicketsByUserId(int.Parse(id));
        if (genres is not null && genres.Count > 0)
            return Ok(genres);
        return BadRequest("Dont have category in system");
    }
    
    [HttpPost("add")]
    public async Task<IActionResult> AddCategory([FromBody] CreateTicketDTO request)
    {
        var response = await _ticketService.AddTicket(request);
        if (response.IsSuccess)
            return Ok(response);
        return BadRequest(response);
    }
}