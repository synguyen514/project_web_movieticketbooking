﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MTB.Entity.DTOs;
using MTB.Entity.Models;
using MTB.Services;
using AuthenticationDTO = MTB.Entity.DTOs.AuthenticationDTO;

namespace MTB.APIs.Controllers;

[Route("api/authentication-management")]
[ApiController]
public class AuthenticationController : ControllerBase
{
    private readonly IJwtService _jwtService;
    private readonly IConfiguration _configuration;
    private readonly Movie_Ticket_BookingContext _context;

    public AuthenticationController(IJwtService jwtService, IConfiguration configuration, Movie_Ticket_BookingContext context)
    {
        _jwtService = jwtService;
        _configuration = configuration;
        _context = context;
    }
    
    [HttpPost("login")]
    public async Task<IActionResult> Login(AuthenticationDTO authentication)
    {
        var auth = await _jwtService.Authentication(authentication);
        return Ok(auth);
    }

    [HttpPost("logout")]
    public async Task<IActionResult> Logout(Guid id)
    {
        var result = await _jwtService.Logout(id);
        if (result.IsSuccess) return Ok(result);
        return BadRequest("Logout failed");
    }

    [HttpPost("RenewToken")]
    public async Task<IActionResult> RenewToken(TokenResponse tokenmodel)
    {
        var jwtTokenHandler = new JwtSecurityTokenHandler();
        var secretkeyByte = Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]);
        var secretKey = Encoding.UTF8.GetBytes(_configuration["Jwt:Key"] ?? string.Empty);
        var tokenValidateParameter = new TokenValidationParameters
        {
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateIssuerSigningKey = true,
            ClockSkew = TimeSpan.Zero,
            IssuerSigningKey = new SymmetricSecurityKey(secretKey),
            ValidateLifetime = false
        };
        try
        {
            var tokenInVerification = jwtTokenHandler.ValidateToken(tokenmodel.AccessToken
                , tokenValidateParameter, out var validatedToken);
            if (validatedToken is JwtSecurityToken jwtSecurityToken)
            {
                var result = jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256);
                if (!result) return BadRequest("Invalid Token");
            }

            var refresh = await _context.TokenJwts.SingleOrDefaultAsync(rf => rf.Token.Equals(tokenmodel.RefreshToken));

            if (refresh == null)
                return BadRequest("Refresh token does not exist");
            if ((bool)refresh.IsRevoked!)
                return BadRequest("Refresh token is revoked");
            if (refresh.JwtId != tokenInVerification.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti)
                    ?.Value) return BadRequest("Token does not match each other");
            //check access het han chua
            if (validatedToken.ValidTo > DateTime.UtcNow) return BadRequest("current access token hasn't expired yet");
            var uid = tokenInVerification.Claims.FirstOrDefault(x => x.Type.Equals("UserId"))?.Value;

            var user = await _context.Users.SingleOrDefaultAsync(a =>
                uid != null && a.Id.ToString().ToLower().Equals(uid.ToLower()));
            if (user == null) return StatusCode(500);
            var tokeRefresh = _jwtService.GenerateAccessToken(user);
            return Ok(tokeRefresh);
        }
        catch
        {
            return StatusCode(500);
        }
    }
    
    [HttpPost("send-otp")]
    public async Task<IActionResult> SendOtpToMail(string mail)
    {
        var response = await _jwtService.GetOtp(mail);
        if (response.IsSuccess)
            return Ok(response);
        return BadRequest(response);
    }
}