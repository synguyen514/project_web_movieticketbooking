using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using VNPAY_CS_ASPX;

namespace MTB.APIs.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PaymentController : ControllerBase
{
    private readonly IConfiguration _configuration;

    public PaymentController(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    [HttpPost("create_payment_url")]
    public IActionResult CreatePaymentUrl([FromBody] PaymentRequest request)
    {
        var vnp_TmnCode = _configuration["Vnpay:TmnCode"];
        var vnp_HashSecret = _configuration["Vnpay:HashSecret"];
        var vnp_Url = _configuration["Vnpay:Url"];
        var vnp_ReturnUrl = _configuration["Vnpay:ReturnUrl"];
        VnPayLibrary vnpay = new VnPayLibrary();
        vnpay.AddRequestData("vnp_Version", VnPayLibrary.VERSION);
        vnpay.AddRequestData("vnp_Command", "pay");
        vnpay.AddRequestData("vnp_TmnCode", vnp_TmnCode);
        vnpay.AddRequestData("vnp_Amount",(request.Amount * 100).ToString(CultureInfo.InvariantCulture));
        vnpay.AddRequestData("vnp_BankCode", request.BankCode); 
        vnpay.AddRequestData("vnp_CreateDate", DateTime.Now.ToString("yyyyMMddHHmmss"));
        vnpay.AddRequestData("vnp_CurrCode", "VND");
        vnpay.AddRequestData("vnp_IpAddr", HttpContext.Connection.RemoteIpAddress?.ToString()!);
        vnpay.AddRequestData("vnp_Locale", "vn");
        vnpay.AddRequestData("vnp_OrderInfo", "Thanh toan don hang:" + request.OrderInfo);
        vnpay.AddRequestData("vnp_OrderType", "other"); 
        vnpay.AddRequestData("vnp_ReturnUrl", vnp_ReturnUrl);
        vnpay.AddRequestData("vnp_TxnRef",DateTime.Now.Ticks.ToString());
        string paymentUrl = vnpay.CreateRequestUrl(vnp_Url, vnp_HashSecret);
        return Ok(new { paymentUrl });
    }
}

public class PaymentRequest
{
    public decimal Amount { get; set; }
    public string OrderInfo { get; set; }
    public string BankCode { get; set; }

}
