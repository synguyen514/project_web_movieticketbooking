﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Threading.Tasks;
using MTB.Entity.DTOs;
using MTB.Entity.Models;
using MTB.Services;

namespace MTB.APIs.Controllers
{
    //[Authorize]
    [Route("api/file-management")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IImageService _imageService;
        private readonly Movie_Ticket_BookingContext _context;

        public FileController(IImageService imageService, Movie_Ticket_BookingContext context)
        {
            _imageService = imageService;
            _context = context;
        }

        [HttpPost("upload")]
        public async Task<IActionResult> UploadListFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return BadRequest("No file uploaded.");

            // Tìm thư mục của dự án Angular
            //var currentDirectory = Directory.GetCurrentDirectory();
            var projectRoot = Directory.GetCurrentDirectory()
                .Substring(0, Directory.GetCurrentDirectory().IndexOf("MTB.APIs"));
            if (projectRoot == null)
                return BadRequest("Cannot determine project root directory.");

            // Đường dẫn tới thư mục assets/images trong dự án Angular
            var angularAssetsPath = Path.Combine(projectRoot, "MTB.Client", "src", "app", "assets", "images");
            if (!Directory.Exists(angularAssetsPath))
                Directory.CreateDirectory(angularAssetsPath);

            var filePath = Path.Combine(angularAssetsPath, file.FileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            var image = new ImageDTO
            {
                ImageName = file.FileName,
            };

            var resultCreateImage = await _imageService.CreateImage(image);
            if (!resultCreateImage.IsSuccess)
                return BadRequest("Create new image in DB failed");
            return Ok(new
            {
                IsSuccess = true,
                Message = "Upload file complete",
                ImageId = resultCreateImage.ImageId
            });
        }
    }
}