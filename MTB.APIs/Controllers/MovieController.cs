using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MTB.Entity.Models;
using MTB.Services;

namespace MTB.APIs.Controllers;

//[Authorize]
[Route("api/movie-management")]
[ApiController]
public class MovieController : ControllerBase
{
    private readonly IMoviesService _moviesService;

    public MovieController(IMoviesService moviesService)
    {
        _moviesService = moviesService;
    }
    
    [HttpGet("get-all")]
    public async Task<IActionResult> GetAllMovies()
    {
        var movies = await _moviesService.GetAllMovies();
        if (movies is not null && movies.Count > 0)
            return Ok(movies);
        return BadRequest("Dont have movies in system");
    }
    
    [HttpGet("get-all-by-genre/{id}")]
    public async Task<IActionResult> GetAllMoviesByGenreId(string id)
    {
        var movies = await _moviesService.GetAllMoviesByGenreId(int.Parse(id));
        if (movies is not null && movies.Count > 0)
            return Ok(movies);
        return BadRequest("Dont have category in system");
    }

    [HttpGet("detail-by-id/{id}")]
    public async Task<IActionResult> GetCategoryById(Guid id)
    {
        return Ok();
    }

    [HttpPost("add")]
    public async Task<IActionResult> AddCategory()
    {
        return Ok();
    }

    [HttpPut("update")]
    public async Task<IActionResult> UpdateCategory()
    {
        return Ok();
    }


    [HttpPut("delete/{id}")]
    public async Task<IActionResult> DeleteCategory(string id)
    {
        return Ok();
    }
}