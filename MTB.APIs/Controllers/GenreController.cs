using Microsoft.AspNetCore.Mvc;
using MTB.Services;

namespace MTB.APIs.Controllers;

//[Authorize]
[Route("api/genre-management")]
[ApiController]
public class GenreController : ControllerBase
{
    private readonly IGenreService _genreService;

    public GenreController(IGenreService genreService)
    {
        _genreService = genreService;
    }
    
    [HttpGet("get-all")]
    public async Task<IActionResult> GetAllMovies()
    {
        var genres = await _genreService.GetAllGenres();
        if (genres is not null && genres.Count > 0)
            return Ok(genres);
        return BadRequest("Dont have genre in system");
    }
}