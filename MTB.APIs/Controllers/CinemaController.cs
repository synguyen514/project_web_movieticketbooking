using Microsoft.AspNetCore.Mvc;
using MTB.Services;

namespace MTB.APIs.Controllers;

//[Authorize]
[Route("api/cinema-management")]
[ApiController]
public class CinemaController : ControllerBase
{
    private readonly ICinemaService _cinemaService;

    public CinemaController(ICinemaService cinemaService)
    {
        _cinemaService = cinemaService;
    }
    
    [HttpGet("get-all")]
    public async Task<IActionResult> GetAllMovies()
    {
        var genres = await _cinemaService.GetAllCinema();
        if (genres is not null && genres.Count > 0)
            return Ok(genres);
        return BadRequest("Dont have cinema in system");
    }
}