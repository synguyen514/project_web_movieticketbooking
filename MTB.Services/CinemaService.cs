using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MTB.Entity.DTOs;
using MTB.Entity.DTOs.MovieDTO;
using MTB.Entity.Models;

namespace MTB.Services;

public interface ICinemaService
{
    Task<List<CinemaDTO>?> GetAllCinema();
}
public class CinemaService : ICinemaService
{
    private readonly IConfiguration _configuration;
    private readonly Movie_Ticket_BookingContext _context;

    public CinemaService(IConfiguration configuration, Movie_Ticket_BookingContext context)
    {
        _configuration = configuration;
        _context = context;
    }
    
    public async Task<List<CinemaDTO>?> GetAllCinema()
    {
        var lstMovies = await _context.Cinemas
            .Where(x => (bool)(!x.IsDeleted)!)
            .ToListAsync();
        return lstMovies.Select(cinema => new CinemaDTO
        {
            Id = cinema.Id,
            CinemaName = cinema.CinemaName,
            Address = cinema.Address,
            IsDeleted = cinema.IsDeleted
        }).ToList();
    }
}