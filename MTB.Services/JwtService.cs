﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MTB.Entity.DTOs;
using MTB.Entity.Models;
using WMS.Entity.DTOs;
using WMS.Entity.DTOs.UserDTO;
using WMS.Shared.Extensions;

namespace MTB.Services;

public interface IJwtService
{
    string HashMd5(string input);
    Task<TokenResponse> Authentication(AuthenticationDTO authentication);
    Task<TokenResponse> GenerateAccessToken(User user);
    Task<BaseResponseDto> Logout(Guid id);
    Task<OTPResponse> GetOtp(string mail);
}

public class JwtService: IJwtService
{
    private readonly IConfiguration _configuration;
    private readonly Movie_Ticket_BookingContext _context;
    private readonly IMailSmtpExtension _mailSmtpExtension;

    public JwtService(IConfiguration configuration, Movie_Ticket_BookingContext context, IMailSmtpExtension mailSmtpExtension)
    {
        _configuration = configuration;
        _context = context;
        _mailSmtpExtension = mailSmtpExtension;
    }
    public string HashMd5(string input)
    {
        MD5 md5 = MD5.Create();
        byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
        StringBuilder hashSb = new StringBuilder();
        foreach (byte b in hash)
        {
            hashSb.Append(b.ToString("X2"));
        }

        return hashSb.ToString();
    }

    public async Task<TokenResponse> Authentication(AuthenticationDTO authentication)
    {
        //get user by email and password
        var user = await _context.Users
            .Include(x => x.Image)
            .Include(x => x.Role)
            .FirstOrDefaultAsync(x => x.Password != null
                                      && x.Email != null
                                      && x.Email.ToLower().Trim()
                                          .Equals(authentication.Email.ToLower().Trim())
                                      && x.Password.ToLower().Equals(HashMd5(authentication.Password).ToLower())
                                      && (bool)(!x.IsDeleted)!);

        if (user != null)
        {
            var token = await GenerateAccessToken(user);
            return token;
        }

        return null;
    }

    public async Task<TokenResponse> GenerateAccessToken(User user)
    {
        var jwtTokenHandler = new JwtSecurityTokenHandler();
        var secretKey = Encoding.UTF8.GetBytes(_configuration["Jwt:Key"] ?? string.Empty);

        var tokenDescription = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new[]
            {
                new Claim("UserId", user.Id.ToString()),
                new Claim("RoleId", user.RoleId.ToString() ?? string.Empty),
                new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString())
            }),
            Expires = DateTime.UtcNow.AddMonths(1),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKey),
                SecurityAlgorithms.HmacSha512Signature)
        };

        var token = jwtTokenHandler.CreateToken(tokenDescription);
        var accessToken = jwtTokenHandler.WriteToken(token);
        var refreshToken = GenerateRefreshToken();

        var tokenJwt = new TokenJwt
        {
            JwtId = token.Id,
            Token = refreshToken,
            UserId = user.Id,
            IsUsed = false,
            IsRevoked = false,
            IssueAt = DateTime.UtcNow,
            ExpireAt = DateTime.UtcNow.AddMinutes(30)
        };

        var userToken = await _context.TokenJwts
            .FirstOrDefaultAsync(x => x.UserId.ToString().ToLower().Equals(user.Id.ToString().ToLower()));
        if (userToken is null)
        {
            //add infomation token jwt in db
            await _context.TokenJwts.AddAsync(tokenJwt);
            await _context.SaveChangesAsync();
        }
        else
        {
            userToken.Token = tokenJwt.Token;
            _context.TokenJwts.Update(userToken);
            await _context.SaveChangesAsync();
        }

        var userDTO = new UserAuthDto
        {
            Id = user.Id,

            FullName = user.FullName,

            Dob = user.Dob,

            Gender = user.Gender,

            Address = user.Address,

            Phone = user.Phone,

            Email = user.Email,

            Password = user.Password,

            RoleId = user.RoleId,

            ImageName = user.Image?.ImageName,

            RoleName = user.Role?.RoleName
        };
        return new TokenResponse
        {
            AccessToken = accessToken,
            RefreshToken = GenerateRefreshToken(),
            User = userDTO
        };
    }

    private string GenerateRefreshToken()
    {
        var random = new byte[32];
        using (var r = RandomNumberGenerator.Create())
        {
            r.GetBytes(random);
            return Convert.ToBase64String(random);
        }
    }

    public async Task<BaseResponseDto> Logout(Guid id)
    {
        var token = await _context.TokenJwts
            .FirstOrDefaultAsync(x => x.UserId.ToString().ToLower().Equals(id.ToString().ToLower()));

        if (token == null)
        {
            return new BaseResponseDto
            {
                IsSuccess = false,
                Message = "Token not exist"
            };
        }

        _context.TokenJwts.Remove(token);
        await _context.SaveChangesAsync();

        return new BaseResponseDto
        {
            IsSuccess = true,
            Message = "Logout is success"
        };
    }

    public async Task<OTPResponse> GetOtp(string mail)
    {
        var user = await _context.Users
            .Where(u => u.Email != null && u.Email.ToLower() == mail.ToLower() && (bool)(!u.IsDeleted)!)
            .FirstOrDefaultAsync();
        if (user is null)
            return new OTPResponse { IsSuccess = false, Message = "Mail not exist in system" };

        var defaultPassword = _mailSmtpExtension.GeneratePassword();
        if (user.FullName != null)
            await _mailSmtpExtension.Send(mail, defaultPassword, "Forgot Password", user.FullName);

        return new OTPResponse { IsSuccess = true, Message = "Send otp success", DefaultPassword = defaultPassword };
    }
}