using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MTB.Entity.DTOs.MovieDTO;
using MTB.Entity.Models;

namespace MTB.Services;

public interface IGenreService
{
    Task<List<GenreDTO>?> GetAllGenres();
}

public class GenreService: IGenreService
{
    private readonly IConfiguration _configuration;
    private readonly Movie_Ticket_BookingContext _context;

    public GenreService(IConfiguration configuration, Movie_Ticket_BookingContext context)
    {
        _configuration = configuration;
        _context = context;
    }
    
    public async Task<List<GenreDTO>?> GetAllGenres()
    {
        var lstMovies = await _context.Genres
            .Where(x => (bool)(!x.IsDeleted)!)
            .ToListAsync();
        return lstMovies.Select(genre => new GenreDTO
        {
            Id = genre.Id,
            GenreName = genre.GenreName,
            IsDeleted = genre.IsDeleted
        }).ToList();
    }
}