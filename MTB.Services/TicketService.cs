using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MTB.Entity.DTOs;
using MTB.Entity.DTOs.MovieDTO;
using MTB.Entity.Models;
using WMS.Entity.DTOs;

namespace MTB.Services;

public interface ITicketsService
{
    Task<List<TicketDTO>?> GetAllTickets();
    Task<List<TicketDTO>?> GetAllTicketsByUserId(int id);
    Task<BaseResponseDto> AddTicket(CreateTicketDTO request);
}

public class TicketService : ITicketsService
{
    private readonly IConfiguration _configuration;
    private readonly Movie_Ticket_BookingContext _context;

    public TicketService(IConfiguration configuration, Movie_Ticket_BookingContext context)
    {
        _configuration = configuration;
        _context = context;
    }

    public async Task<List<TicketDTO>?> GetAllTickets()
    {
        var lstTicket = await _context.Tickets
            .Where(x => (bool)(!x.IsDeleted)!)
            .Include(ticket => ticket.Movie)
            .Include(ticket => ticket.Cinema)
            .ToListAsync();
        return lstTicket.Select(ticket => new TicketDTO
        {
            Id = ticket.Id,
            UserId = ticket.UserId,
            MovieId = ticket.MovieId,
            MovieName = ticket.Movie?.MovieName,
            CinemaId = ticket.CinemaId,
            CinemaName = ticket.Cinema?.CinemaName,
            DateOrder = ticket.DateOrder,
            Price = ticket.Price,
            SeatNumber = ticket.SeatNumber,
            HallNumber = ticket.HallNumber,
            Status = ticket.Status,
            IsDeleted = ticket.IsDeleted
        }).ToList();
    }

    public async Task<List<TicketDTO>?> GetAllTicketsByUserId(int id)
    {
        var lstTicket = await _context.Tickets
            .Where(x => (bool)(!x.IsDeleted)! && x.UserId.Equals(id))
            .Include(ticket => ticket.Movie)
            .Include(ticket => ticket.Cinema)
            .ToListAsync();
        return lstTicket.Select(ticket => new TicketDTO
        {
            Id = ticket.Id,
            UserId = ticket.UserId,
            MovieId = ticket.MovieId,
            MovieName = ticket.Movie?.MovieName,
            CinemaId = ticket.CinemaId,
            CinemaName = ticket.Cinema?.CinemaName,
            DateOrder = ticket.DateOrder,
            Price = ticket.Price,
            SeatNumber = ticket.SeatNumber,
            HallNumber = ticket.HallNumber,
            Status = ticket.Status,
            IsDeleted = ticket.IsDeleted
        }).ToList();
    }

    public async Task<BaseResponseDto> AddTicket(CreateTicketDTO request)
    {
        try
        {
            var category = new Ticket
            {
                UserId = request.UserId,
                MovieId = request.MovieId,
                CinemaId = request.CinemaId,
                DateOrder = DateTime.Now,
                Price = 120000,
                SeatNumber = request.SeatNumber,
                HallNumber = 1,
                Status = "Thanh toán thành công",
                IsDeleted = false
            };

            await _context.Tickets.AddAsync(category);
            await _context.SaveChangesAsync();
            return new BaseResponseDto { IsSuccess = true, Message = $"Create Ticket success"};
        }
        catch (Exception ex)
        {
            return new BaseResponseDto { IsSuccess = false, Message = $"Create Ticket failed: {ex}" };
        }
    }
}