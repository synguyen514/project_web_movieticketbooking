using Microsoft.Extensions.Configuration;
using MTB.Entity.DTOs;
using MTB.Entity.Models;

namespace MTB.Services;

public interface IImageService
{
    Task<CreateImageResponse> CreateImage(ImageDTO imageRequest);
}

public class ImageService : IImageService
{
    private readonly IConfiguration _configuration;
    private readonly Movie_Ticket_BookingContext _context;

    public ImageService(IConfiguration configuration, Movie_Ticket_BookingContext context)
    {
        _configuration = configuration;
        _context = context;
    }

    public async Task<CreateImageResponse> CreateImage(ImageDTO imageRequest)
    {
        var image = new Image
        {
            ImageName = imageRequest.ImageName,
            CreateDate = DateTime.Now,
            IsDeleted = false
        };
        try
        {
            await _context.Images.AddAsync(image);
            await _context.SaveChangesAsync();
            return new CreateImageResponse
            {
                IsSuccess = true,
                Message = "Image and ImageDetail created successfully",
                ImageId = image.Id.ToString()
            };
        }
        catch (Exception e)
        {
            return new CreateImageResponse
            {
                IsSuccess = false,
                Message = "Image creation failed: " + e.Message,
                ImageId = image.Id.ToString()
            };
        }
    }
}