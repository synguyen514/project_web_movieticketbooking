using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MTB.Entity.DTOs.MovieDTO;
using MTB.Entity.Models;
using WMS.Entity.DTOs;

namespace MTB.Services;

public interface IMoviesService
{
    Task<List<MovieDTO>?> GetAllMovies();
    Task<List<MovieDTO>?> GetAllMoviesByGenreId(int id);
}

public class MovieService : IMoviesService
{
    private readonly IConfiguration _configuration;
    private readonly Movie_Ticket_BookingContext _context;

    public MovieService(IConfiguration configuration, Movie_Ticket_BookingContext context)
    {
        _configuration = configuration;
        _context = context;
    }
    
    public async Task<List<MovieDTO>?> GetAllMovies()
    {
        var lstMovies = await _context.Movies
            .Where(x => (bool)(!x.IsDeleted)!)
            .Include(movie => movie.Image)
            .ToListAsync();
        return lstMovies.Select(movie => new MovieDTO
        {
            Id = movie.Id,
            MovieName = movie.MovieName,
            YearOfManufacture = movie.YearOfManufacture,
            ReleaseDate = movie.ReleaseDate,
            Description = movie.Description,
            ImageId = movie.ImageId,
            ImageNameUrl = movie.Image?.ImageName,
            TrailerUrl = movie.TrailerUrl,
            GenreId = movie.GenreId,
            CountryId = movie.CountryId,
            IsDeleted = movie.IsDeleted
        }).ToList();
    }
    
    public async Task<List<MovieDTO>?> GetAllMoviesByGenreId(int id)
    {
        var lstMovies = await _context.Movies
            .Where(x => (bool)(!x.IsDeleted)! && x.GenreId.Equals(id))
            .Include(movie => movie.Image)
            .ToListAsync();
        return lstMovies.Select(movie => new MovieDTO
        {
            Id = movie.Id,
            MovieName = movie.MovieName,
            YearOfManufacture = movie.YearOfManufacture,
            ReleaseDate = movie.ReleaseDate,
            Description = movie.Description,
            ImageId = movie.ImageId,
            ImageNameUrl = movie.Image?.ImageName,
            TrailerUrl = movie.TrailerUrl,
            GenreId = movie.GenreId,
            CountryId = movie.CountryId,
            IsDeleted = movie.IsDeleted
        }).ToList();
    }
}