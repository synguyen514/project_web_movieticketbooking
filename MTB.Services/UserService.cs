using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MTB.Entity.DTOs.UserDTO;
using MTB.Entity.Models;
using WMS.Entity.DTOs;
using WMS.Entity.DTOs.UserDTO;

namespace MTB.Services;

public interface IUserService
{
    Task<BaseResponseDto> UpdatePassUser(UpdateUserDto request);
    Task<BaseResponseDto> UpdateImageUser(UpdateUserImage request);
}

public class UserService : IUserService
{
    private readonly IConfiguration _configuration;
    private readonly Movie_Ticket_BookingContext _context;

    public UserService(IConfiguration configuration, Movie_Ticket_BookingContext context)
    {
        _configuration = configuration;
        _context = context;
    }

    public async Task<BaseResponseDto> UpdatePassUser(UpdateUserDto request)
    {
        try
        {
            var existedUser = await _context.Users.FirstOrDefaultAsync(x => x.Email != null &&
                                                                            x.Email.ToLower()
                                                                                .Equals(request.Email.ToLower()) &&
                                                                            (bool)(!x.IsDeleted)!);

            if (existedUser == null) return new BaseResponseDto { IsSuccess = false, Message = "User not exist" };
            existedUser.Password = request.Password;
            _context.Update(existedUser);
            await _context.SaveChangesAsync();
        }
        catch (Exception e)
        {
            return new BaseResponseDto { IsSuccess = false, Message = "Update user failed: " + e.Message };
        }

        return new BaseResponseDto { IsSuccess = true, Message = "Update user success!" };
    }
    
    public async Task<BaseResponseDto> UpdateImageUser(UpdateUserImage request)
    {
        try
        {
            var existedUser = await _context.Users.FirstOrDefaultAsync(x => x.Id.Equals(request.Id) &&
                                                                            (bool)(!x.IsDeleted)!);

            if (existedUser == null) return new BaseResponseDto { IsSuccess = false, Message = "User not exist" };
            existedUser.ImageId = request.ImageId;
            _context.Update(existedUser);
            await _context.SaveChangesAsync();
        }
        catch (Exception e)
        {
            return new BaseResponseDto { IsSuccess = false, Message = "Update user failed: " + e.Message };
        }

        return new BaseResponseDto { IsSuccess = true, Message = "Update user success!" };
    }
}